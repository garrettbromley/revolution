-- Create and use the revolution database
-- CREATE DATABASE IF NOT EXISTS `revolution` DEFAULT CHARACTER SET utf8;
USE `revolution`;

-- Create the players table
CREATE TABLE IF NOT EXISTS `players` (
    `uid` int(6) NOT NULL AUTO_INCREMENT,
    `name` varchar(32) NOT NULL,
    `pid` varchar(17) NOT NULL,
    `cash` int(100) NOT NULL DEFAULT '0',
    `bank` int(100) NOT NULL DEFAULT '0',
    `creditBalance` int(100) NOT NULL DEFAULT '0',
    `creditLimit` int(100) NOT NULL DEFAULT '0',
    `coplevel` enum('0','1','2','3','4','5','6','7') NOT NULL DEFAULT '0',
    `mediclevel` enum('0','1','2','3','4','5') NOT NULL DEFAULT '0',
    `civ_licenses` text NOT NULL,
    `cop_licenses` text NOT NULL,
    `med_licenses` text NOT NULL,
    `civ_gear` text NOT NULL,
    `cop_gear` text NOT NULL,
    `med_gear` text NOT NULL,
    `active_gear` varchar(5) NOT NULL DEFAULT '"civ"',
    `active_role` varchar(5) NOT NULL DEFAULT '"civ"',
    `stats` varchar(32) NOT NULL DEFAULT '"[100,100,0]"',
    `arrested` tinyint(1) NOT NULL DEFAULT '0',
    `adminlevel` enum('0','1','2','3','4','5') NOT NULL DEFAULT '0',
    `donorlevel` enum('0','1','2','3','4','5') NOT NULL DEFAULT '0',
    `alive` tinyint(1) NOT NULL DEFAULT '0',
    `position` varchar(64) NOT NULL DEFAULT '"[]"',
    `playtime` int(100) NOT NULL DEFAULT '0',
    `blacklist` tinyint(1) NOT NULL DEFAULT '0',
    `insert_time` timestamp DEFAULT CURRENT_TIMESTAMP,
    `last_seen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`uid`),
    UNIQUE KEY `pid` (`pid`),
    KEY `name` (`name`),
    KEY `blacklist` (`blacklist`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 10 ;

-- Create the vehicles table
CREATE TABLE IF NOT EXISTS `vehicles` (
    `id` int(6) NOT NULL AUTO_INCREMENT,
    `side` varchar(16) NOT NULL,
    `classname` varchar(64) NOT NULL,
    `type` varchar(16) NOT NULL,
    `pid` varchar(17) NOT NULL,
    `alive` tinyint(1) NOT NULL DEFAULT '1',
    `blacklist` tinyint(1) NOT NULL DEFAULT '0',
    `active` tinyint(1) NOT NULL DEFAULT '0',
    `plate` int(20) NOT NULL,
    `color` int(20) NOT NULL,
    `tuningData` varchar(300) NOT NULL DEFAULT '[]',
    `inventory` text NOT NULL,
    `gear` text NOT NULL,
    `fuel` double NOT NULL DEFAULT '1',
    `damage` varchar(256) NOT NULL,
    `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `side` (`side`),
    KEY `pid` (`pid`),
    KEY `type` (`type`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 2 ;

CREATE TABLE IF NOT EXISTS `houses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` varchar(32) NOT NULL,
  `pos` varchar(64) DEFAULT NULL,
  `owned` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`,`pid`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 4;

-- Create the wanted table
CREATE TABLE IF NOT EXISTS `wanted` (
    `wantedID` varchar(64) NOT NULL,
    `wantedName` varchar(32) NOT NULL,
    `wantedCrimes` text NOT NULL,
    `wantedBounty` int(100) NOT NULL,
    `active` tinyint(1) NOT NULL DEFAULT '0',
    `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`wantedID`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

-- Create the transactions table
CREATE TABLE IF NOT EXISTS `transactions` (
    `transactionID` int(10) NOT NULL AUTO_INCREMENT,
    `fromID` varchar(17) NOT NULL,
    `toID` varchar(17) NOT NULL,
    `amount` int(100) NOT NULL DEFAULT '0',
    `description` varchar(50) NOT NULL,
    `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`transactionID`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

-- Create the fines table
CREATE TABLE IF NOT EXISTS `fines` (
    `fineID` int(10) NOT NULL AUTO_INCREMENT,
    `pid` varchar(17) NOT NULL,
    `name` varchar(32) NOT NULL,
    `amount` int(100) NOT NULL DEFAULT '0',
    `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `paid` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`fineID`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

-- Create the transactions table
CREATE TABLE IF NOT EXISTS `purchases` (
    `purchaseID` int(10) NOT NULL AUTO_INCREMENT,
    `who` varchar(17) NOT NULL,
    `store` varchar(50) NOT NULL,
    `amount` int(100) NOT NULL DEFAULT '0',
    `insert_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`purchaseID`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

CREATE TABLE IF NOT EXISTS `businesses` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `owner` varchar(32) DEFAULT NULL,
    `name` varchar(32) DEFAULT NULL,
    `members` text,
    `bank` int(100) DEFAULT '0',
    `active` tinyint(4) DEFAULT '1',
    PRIMARY KEY (`id`),
    UNIQUE KEY `name_UNIQUE` (`name`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

CREATE TABLE IF NOT EXISTS `containers` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `pid` varchar(32) NOT NULL,
    `classname` varchar(32) NOT NULL,
    `pos` varchar(64) DEFAULT NULL,
    `inventory` varchar(500) NOT NULL,
    `gear` text NOT NULL,
    `dir` varchar(64) DEFAULT NULL,
    `active` tinyint(1) NOT NULL DEFAULT '0',
    `owned` tinyint(4) DEFAULT '0',
    PRIMARY KEY (`id`,`pid`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

CREATE TABLE IF NOT EXISTS `candidates` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `election` int(11) DEFAULT NULL,
    `pid` varchar(32) DEFAULT NULL,
    `name` varchar(32) DEFAULT NULL,
    `votes` int(3) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

CREATE TABLE IF NOT EXISTS `economy` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `commodity` varchar(32) DEFAULT NULL,
    `price` int(100) DEFAULT '0',
    PRIMARY KEY (`id`)
) DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
