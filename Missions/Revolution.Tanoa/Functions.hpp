class Processing {
    tag = "Process";
    class Process {
        file = "core\functions\process";
        class requestData {};
        class newPlayerSetup {};
        class playerCheck {};
        class playerData {};
        class compareArrays {};
        class gearDifferences {};
    }
};

class Updating {
    tag = "Update";
    class Update {
        file = "core\functions\update";
        class allData {};
        class gear {};
        class licenses {};
        class money {};
        class position {};
    }
};

class Banking {
    tag = "Bank";
    class Bank {
        file = "core\functions\bank";
        class sendMoney {};
        class receiveMoney {};
    };
};

class Fine {
    tag = "fines";
    class Fines {
        file = "core\functions\fines";
        class addFine {};
        class payFines {};
        class requestFines {};
        class getFines {};
    };
};

class Miscellaneous {
    tag = "Misc";
    class Misc {
        file = "core\functions\misc";
        class introShot {};
        class stringReplace {};
        class tazeSound {};
        class handcuffSound {};
        class animationSync {};
        class statusBar {};
        class loadSpeedCameras {postInit = 1; headerType = -1;};
        class leavePlayerArea {};
        class clamp {};
        class formatNumber {};
    };
};

class RadioChannels {
    tag = "Radio";
    class Radio {
        file = "core\functions\radio";
        class initialize {};
        class change {};
    };
};

class GearChanges {
    tag = "Gear";
    class Gear {
        file = "core\functions\gear";
        class saveGear {};
        class assignGear {};
        class toCop {};
        class toMed {};
        class toCiv {};
    };
};

class RoleChanges {
    tag = "Role";
    class Role {
        file = "core\functions\roles";
        class toCop {};
        class toMed {};
        class toCiv {};
    };
};

class Shopping {
    tag = "Shop";
    class Shops {
        file = "core\functions\shops";
        class openArsenalShop {};
        class flushArsenal {};
        class processDiff {};
        class getPrices {};
        class confirmPurchase {};
        class addPurchase {};
        class civClothingShop {};
        class civGunShop {};
        class copShop {};
    }
};

class overrideVATemplates {
	tag = "arsenalFunctions";
	class Inventory {
		file = "core\functions\arsenal";
		class initOverride { postInit = 1; };
		class loadInventory_whiteList {};
		class overrideVAButtonDown {};
		class overrideVATemplateOK {};
	};
};

class Civilians {
    tag = "civ";
    class Civ {
        file = "core\functions\civilian";
        class tazed {};
        class restrained {};
    };
};

class Cops {
    tag = "cop";
    class Cop {
        file = "core\functions\cop";
        class sirenLights {};
        class copLights {};
        class siren {};
        class radar {};
        class restrainAction {};
        class copMarkers {};
        class policeMarkers {};
    };
};

class Handlers {
    tag = "handle";
    class Handle {
        file = "core\functions\handlers";
        class init { postInit = 1; };
        class handleDamage {};
        class onKilled {};
        class keyHandler {};
    };
};

class Indicators {
	tag = "indicators";
	class functions {
		file = "core\functions\vehicles\indicators";
		class getVehicleIndicatorOffsets {};
		class enableIndicator {};
		class disableIndicator {};
	};
};

class Admins {
    tag = "admin";
    class Admin {
        file = "core\functions\admin";
        class init { postInit = 1; };
        class execute {};
        class removeActions {};
        class adminMarkers {};
        class adminMarkersOff {};
        class godOn {};
        class godOff {};
        class healSelf {};
        class repairBuild {};
        class teleport {};
        class vehDelete {};
        class vehGodOn {};
        class vehGodOff {};
        class vehRepair {};
    };
};

class Action {
    tag = "actions";
    class Actions {
        file = "core\functions\actions";
        class surrender {};
    };
};

class Menu {
	tag = "Menu";
	class Menu {
		file = "core\functions\menus";
		class keydown {};
	};
	
};

class Atm {
    tag = "Atm";
    class Atm {
        file = "core\functions\menus\atm";
        class open {};
        class deposit {};
        class withdraw {};
        class transfer {};
    };
};

class QuestSystem {
    tag = "QST";
    class Quests {
        file = "core\functions\quest-system";
        class fetchQuestStatus {};
        class getMissionData {};
        class initializeQuestSystem {};
        class startMission {};
        class startNPCConversation {};
        class startQuestDaemons {};
    };
}

class Inventory {
    tag = "Inv";
    class Inv {
        file = "core\functions\menus\inventory";
        class open {};
		class update {};
		class selectItem {};
		class toBackpack {};
		class toHands {};
    };
};

class Cinematics {
    tag = "Cinematic";
    class Intro {
        file = "core\functions\cinematic\intro";
        class loginCinematic {};
        class stopLoginCinematic {};
    };

    class Utils {
        file = "core\functions\cinematic\util";
        class cameraCalculateTarget {};
        class circunstancialBlur {};
        class circunstancialMonochrome {};
        class createDeadCamera {};
        class createFixedCamera {};
        class deleteDeadCamera {};
        class deleteFixedCamera {};
        class endBlur {};
        class findFixedCamera {};
        class killAllCircunstancialBlurs {};
        class startBlur {};
        class updateFixedCamera {};
    };
};

class Music {
    tag = "Music";
    class Manager {
        file = "core\functions\music";
        class playRandom {};
        class playSong {};
        class stopMusic {};
    };
};