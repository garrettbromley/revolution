/*
    File: onPlayerRespawn.sqf
    Author: Garrett
    Description:
        For when they respawn
*/

waitUntil { !isNil "isAlive"; };

if (isAlive == 1) exitWith {diag_log "Exiting onPlayerRespawn"; };

titleCut ["", "BLACK FADED", 999];

// If they are coming to life from being killed
private ["_startingPos", "_startingDir", "_startingUniforms", "_setRating", "_getRating", "_addVal"];
_startingPos = call compile ((MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "startingPos") call BIS_fnc_getCfgData);
_startingDir = (MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "startingDir") call BIS_fnc_getCfgData;


// Wait until they spawn
waitUntil { !isNull player && player == player; };

// Setup Misc Default Variables
player setVariable ["isCop", false, true];
stats       = [100, 100, 0];
arrested    = 0;
isAlive     = 1;
isTazed     = false;
isKnockedOut= false;
cPosition   = _startingPos;
playtime    = 0;

// Pick a random uniform from what we allow
_startingUniforms = call compile ((MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "startingUniforms") call BIS_fnc_getCfgData);
removeUniform player;
player forceAddUniform (selectRandom _startingUniforms);

// Put the player where the new spawn is
player setPosATL _startingPos;
player setDir _startingDir;

// Save their gear
civGear = format ["%1", getUnitLoadout player];
copGear = call compile ((MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "defaultCopGear") call BIS_fnc_getCfgData);
medGear = call compile ((MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "defaultMedGear") call BIS_fnc_getCfgData);
activeGear = "civ";
activeRole = "civ";

// Have the server save the player's data
[] call update_fnc_allData;

// Setup Radio Channels
[] call radio_fnc_initialize;

// Setup Player
player enableFatigue false;
player enableStamina false;
player setCustomAimCoef 0;
_setRating = 999999;
_getRating = rating player;
_addVal = _setRating - _getRating;
player addRating _addVal;

player addItem "ItemGPS"; player assignItem "ItemGPS";

[] spawn misc_fnc_introShot;