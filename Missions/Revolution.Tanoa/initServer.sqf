/*
  File: initServer.sqf
  Author: Garrett
  Description:
    Initalizes the Server
*/

// If not the server, exit
if (!(_this select 0)) exitWith {};

// Run server-side systems
[] call compile preprocessFileLineNumbers "\revolution_server\init.sqf";
