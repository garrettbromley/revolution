#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)

class inventory {
	idd = 11000;
	movingEnable = true;
	enableSimulation = true;
	
	onKeyDown = "[_this,11202,'none'] call Menu_fnc_keydown;";
	onUnload = "keydownAmount = nil; invPlayers = nil; selectedItem = nil;";
	onMouseButtonDown = "_this call inv_fnc_selectItem;";

	class controlsBackground {
		class background: IGUIBack
		{
			idc = -1;
			x = 0 * GUI_GRID_W + GUI_GRID_X;
			y = 1 * GUI_GRID_H + GUI_GRID_Y;
			w = 40 * GUI_GRID_W;
			h = 23 * GUI_GRID_H;
			colorBackground[] = {0,0,0,0.8};
		};
	};
	
	class controls {
		///////////////////
		///////////////////
		////           ////
		////  General  ////
		////           ////
		///////////////////
		///////////////////
		class closeBtn: RscButton
		{
			idc = -1;
			text = "X";
			x = 37.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 2 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			onButtonClick = "closeDialog 0;";
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		
		class helpBtn: RscButton
		{
			idc = -1;
			text = "?";
			x = 0.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 2 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		
		
		
		
		
		
		///////////////////
		///////////////////
		////           ////
		////  Actions  ////
		////           ////
		///////////////////
		///////////////////
		class actionsFrm: RscFrame
		{
			idc = -1;
			text = "Action";
			x = 26 * GUI_GRID_W + GUI_GRID_X;
			y = 12.71 * GUI_GRID_H + GUI_GRID_Y;
			w = 11 * GUI_GRID_W;
			h = 9.88426 * GUI_GRID_H;
		};
		
		class useBtn: RscButton
		{
			idc = -1;
			text = "Use";
			x = 27 * GUI_GRID_W + GUI_GRID_X;
			y = 13.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 9 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		
		class dropBtn: RscButton
		{
			idc = -1;
			text = "Drop";
			x = 27 * GUI_GRID_W + GUI_GRID_X;
			y = 15.25 * GUI_GRID_H + GUI_GRID_Y;
			w = 9 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		
		class giveBtn: RscButton
		{
			idc = -1;
			text = "Give to";
			x = 32 * GUI_GRID_W + GUI_GRID_X;
			y = 17 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		class givePlayer: RscCombo
		{
			idc = 11201;
			x = 27 * GUI_GRID_W + GUI_GRID_X;
			y = 20.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 9 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class giveAmount: RscText
		{
			idc = 11202;
			x = 27 * GUI_GRID_W + GUI_GRID_X;
			y = 18.75 * GUI_GRID_H + GUI_GRID_Y;
			w = 9 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorBackground[] = {0.131,0.736,0.131,0.5};
		};
		class giveType: RscCombo
		{
			idc = 11200;
			x = 27 * GUI_GRID_W + GUI_GRID_X;
			y = 17 * GUI_GRID_H + GUI_GRID_Y;
			w = 5 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		
		////////////////////
		////////////////////
		////            ////
		////  Submenus  ////
		////            ////
		////////////////////
		////////////////////
		class keychain: RscButton
		{
			idc = -1;
			text = "Keychain";
			x = 3 * GUI_GRID_W + GUI_GRID_X;
			y = 19.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		class synchronise: RscButton
		{
			idc = -1;
			text = "Sync";
			x = 3 * GUI_GRID_W + GUI_GRID_X;
			y = 21.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		class phone: RscButton
		{
			idc = -1;
			text = "Phone";
			x = 11.5 * GUI_GRID_W + GUI_GRID_X;
			y = 19.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		
		class market: RscButton
		{
			idc = -1;
			text = "Market";
			x = 11.5 * GUI_GRID_W + GUI_GRID_X;
			y = 21.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 8 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		////////////////
		////////////////
		////        ////
		////  Info  ////
		////        ////
		////////////////
		////////////////
		class RestartTimer: RscText
		{
			idc = 11001;
			text = "Next restart in: 00.00.00";
			x = 28.53 * GUI_GRID_W + GUI_GRID_X;
			y = 22.64 * GUI_GRID_H + GUI_GRID_Y;
			w = 11 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		
		class licenses: RscListbox
		{
			idc = 11002;
			x = 26 * GUI_GRID_W + GUI_GRID_X;
			y = 6.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 11 * GUI_GRID_W;
			h = 6 * GUI_GRID_H;
		};
		
		//Money
		class moneyFrm: RscFrame
		{
			idc = -1;
			text = "Money";
			x = 3 * GUI_GRID_W + GUI_GRID_X;
			y = 1 * GUI_GRID_H + GUI_GRID_Y;
			w = 11 * GUI_GRID_W;
			h = 3.5 * GUI_GRID_H;
		};
		class cashTxt: RscText
		{
			idc = -1;
			text = "Inventory:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		class bankTxt: RscText
		{
			idc = -1;
			text = "Bank:";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3 * GUI_GRID_H + GUI_GRID_Y;
			w = 4 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		class cash: RscText
		{
			idc = 11003;
			text = "$ XX.XXX.XXX";
			x = 8 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 5.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		class bank: RscText
		{
			idc = 11004;
			text = "$ XX.XXX.XXX";
			x = 8 * GUI_GRID_W + GUI_GRID_X;
			y = 3 * GUI_GRID_H + GUI_GRID_Y;
			w = 5.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		
		//Stats
		class statsFrm: RscFrame
		{
			idc = -1;
			text = "Stats";
			x = 14.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1 * GUI_GRID_H + GUI_GRID_Y;
			w = 15.5 * GUI_GRID_W;
			h = 3.5 * GUI_GRID_H;
		};
		class needs1txt: RscText
		{
			idc = -1;
			text = "Health:";
			x = 14.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		class needs2txt: RscText
		{
			idc = -1;
			text = "Hunger:";
			x = 14.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		class needs3txt: RscText
		{
			idc = -1;
			text = "";
			x = 22.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		class needs4txt: RscText
		{
			idc = -1;
			text = "";
			x = 22.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		
		class needs1: RscText
		{
			idc = 11005;
			text = "100%";
			x = 18.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 2.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		class needs2: RscText
		{
			idc = 11006;
			text = "100%";
			x = 18.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3 * GUI_GRID_H + GUI_GRID_Y;
			w = 2.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		class needs3: RscText
		{
			idc = 11007;
			text = "";
			x = 26.5 * GUI_GRID_W + GUI_GRID_X;
			y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 2.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		class needs4: RscText
		{
			idc = 11008;
			text = "";
			x = 26.5 * GUI_GRID_W + GUI_GRID_X;
			y = 3 * GUI_GRID_H + GUI_GRID_Y;
			w = 2.5 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
		};
		
		//Players
		class playersFrm: RscFrame
		{
			idc = -1;
			text = "Players";
			x = 26 * GUI_GRID_W + GUI_GRID_X;
			y = 4.64 * GUI_GRID_H + GUI_GRID_Y;
			w = 11 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
		};
		class civsClr: RscPicture
		{
			idc = -1;
			text = "#(argb,8,8,3)color(0.372,0.047,0.569,1)";
			x = 26.33 * GUI_GRID_W + GUI_GRID_X;
			y = 5.16 * GUI_GRID_H + GUI_GRID_Y;
			w = 0.833327 * GUI_GRID_W;
			h = 0.652776 * GUI_GRID_H;
		};
		class copsClr: RscPicture
		{
			idc = -1;
			text = "#(argb,8,8,3)color(0,0,1,1)";
			x = 30.33 * GUI_GRID_W + GUI_GRID_X;
			y = 5.16 * GUI_GRID_H + GUI_GRID_Y;
			w = 0.833327 * GUI_GRID_W;
			h = 0.652776 * GUI_GRID_H;
		};
		class medsClr: RscPicture
		{
			idc = -1;
			text = "#(argb,8,8,3)color(1,0,0,1)";
			x = 34.33 * GUI_GRID_W + GUI_GRID_X;
			y = 5.16 * GUI_GRID_H + GUI_GRID_Y;
			w = 0.833327 * GUI_GRID_W;
			h = 0.652776 * GUI_GRID_H;
		};
		class civs: RscText
		{
			idc = 11009;
			text = "999";
			x = 27 * GUI_GRID_W + GUI_GRID_X;
			y = 5 * GUI_GRID_H + GUI_GRID_Y;
			w = 2.02777 * GUI_GRID_W;
			h = 0.930555 * GUI_GRID_H;
		};
		class cops: RscText
		{
			idc = 11010;
			text = "999";
			x = 31 * GUI_GRID_W + GUI_GRID_X;
			y = 5 * GUI_GRID_H + GUI_GRID_Y;
			w = 2.02777 * GUI_GRID_W;
			h = 0.930555 * GUI_GRID_H;
		};
		class meds: RscText
		{
			idc = 11011;
			text = "999";
			x = 35 * GUI_GRID_W + GUI_GRID_X;
			y = 5 * GUI_GRID_H + GUI_GRID_Y;
			w = 2.02777 * GUI_GRID_W;
			h = 0.930555 * GUI_GRID_H;
		};
		
		/////////////////
		/////////////////
		////         ////
		////  Items  ////
		////         ////
		/////////////////
		/////////////////
		class moveHandsBtn: RscButton
		{
			idc = -1;
			text = "\/";
			x = 11.39 * GUI_GRID_W + GUI_GRID_X;
			y = 9 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.61111 * GUI_GRID_W;
			h = 1.26852 * GUI_GRID_H;
			colorBackground[] = {1,1,1,0.4};
			onButtonClick = "[] call inv_fnc_toHands;";
		};
		class moveBackpackBtn: RscButton
		{
			idc = -1;
			text = "/\";
			x = 9.5 * GUI_GRID_W + GUI_GRID_X;
			y = 9 * GUI_GRID_H + GUI_GRID_Y;
			w = 1.61111 * GUI_GRID_W;
			h = 1.26852 * GUI_GRID_H;
			colorBackground[] = {1,1,1,0.4};
			onButtonClick = "[] call inv_fnc_toBackpack;";
		};
		
		class cursor: RscPicture
		{
			idc = 11100;
			text = "media\icons\cursor.paa";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 5.31 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		
		//Backpack
		class backpackFrm: RscFrame
		{
			idc = -1;
			text = "Backpack";
			x = 3 * GUI_GRID_W + GUI_GRID_X;
			y = 4.62 * GUI_GRID_H + GUI_GRID_Y;
			w = 16.5 * GUI_GRID_W;
			h = 4.0926 * GUI_GRID_H;
		};
		class backpackItem1: RscPicture
		{
			idc = 11110;
			text = "media\icons\empty.paa";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 5.31 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class backpackItem1Amount: RscText
		{
			idc = 11130;
			text = "";
			style = 5;
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 5.31 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		class backpackItem2: RscPicture
		{
			idc = 11111;
			text = "media\icons\empty.paa";
			x = 7.5 * GUI_GRID_W + GUI_GRID_X;
			y = 5.31 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class backpackItem2Amount: RscText
		{
			idc = 11131;
			text = "";
			style = 5;
			x = 7.5 * GUI_GRID_W + GUI_GRID_X;
			y = 5.31 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		class backpackItem3: RscPicture
		{
			idc = 11112;
			text = "media\icons\empty.paa";
			x = 11.5 * GUI_GRID_W + GUI_GRID_X;
			y = 5.31 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class backpackItem3Amount: RscText
		{
			idc = 11132;
			text = "";
			style = 5;
			x = 11.5 * GUI_GRID_W + GUI_GRID_X;
			y = 5.31 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		class backpackItem4: RscPicture
		{
			idc = 11113;
			text = "media\icons\empty.paa";
			x = 15.5 * GUI_GRID_W + GUI_GRID_X;
			y = 5.31 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class backpackItem4Amount: RscText
		{
			idc = 11133;
			text = "";
			style = 5;
			x = 15.5 * GUI_GRID_W + GUI_GRID_X;
			y = 5.31 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		
		//Hands
		class handsFrm: RscFrame
		{
			idc = -1;
			text = "Hands";
			x = 3.03 * GUI_GRID_W + GUI_GRID_X;
			y = 10.38 * GUI_GRID_H + GUI_GRID_Y;
			w = 16.5 * GUI_GRID_W;
			h = 7.5 * GUI_GRID_H;
		};
		class handsItem1: RscPicture
		{
			idc = 11120;
			text = "media\icons\empty.paa";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class handsItem1Amount: RscText
		{
			idc = 11140;
			text = "";
			style = 5;
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		class handsItem2: RscPicture
		{
			idc = 11121;
			text = "media\icons\empty.paa";
			x = 7.5 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class handsItem2Amount: RscText
		{
			idc = 11141;
			text = "";
			style = 5;
			x = 7.5 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		class handsItem3: RscPicture
		{
			idc = 11122;
			text = "media\icons\empty.paa";
			x = 11.5 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class handsItem3Amount: RscText
		{
			idc = 11142;
			text = "";
			style = 5;
			x = 11.5 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		class handsItem4: RscPicture
		{
			idc = 11123;
			text = "media\icons\empty.paa";
			x = 15.5 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class handsItem4Amount: RscText
		{
			idc = 11143;
			text = "";
			style = 5;
			x = 15.5 * GUI_GRID_W + GUI_GRID_X;
			y = 11 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		
		class handsItem5: RscPicture
		{
			idc = 11124;
			text = "media\icons\empty.paa";
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class handsItem5Amount: RscText
		{
			idc = 11144;
			text = "";
			style = 5;
			x = 3.5 * GUI_GRID_W + GUI_GRID_X;
			y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		class handsItem6: RscPicture
		{
			idc = 11125;
			text = "media\icons\empty.paa";
			x = 7.5 * GUI_GRID_W + GUI_GRID_X;
			y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class handsItem6Amount: RscText
		{
			idc = 11145;
			text = "";
			style = 5;
			x = 7.5 * GUI_GRID_W + GUI_GRID_X;
			y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		class handsItem7: RscPicture
		{
			idc = 11126;
			text = "media\icons\empty.paa";
			x = 11.5 * GUI_GRID_W + GUI_GRID_X;
			y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class handsItem7Amount: RscText
		{
			idc = 11146;
			text = "";
			style = 5;
			x = 11.5 * GUI_GRID_W + GUI_GRID_X;
			y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
		class handsItem8: RscPicture
		{
			idc = 11127;
			text = "media\icons\empty.paa";
			x = 15.5 * GUI_GRID_W + GUI_GRID_X;
			y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
		};
		class handsItem8Amount: RscText
		{
			idc = 11147;
			text = "";
			style = 5;
			x = 15.5 * GUI_GRID_W + GUI_GRID_X;
			y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 3.5 * GUI_GRID_W;
			h = 3 * GUI_GRID_H;
			sizeEx = 1 * GUI_GRID_H;
		};
	};
};