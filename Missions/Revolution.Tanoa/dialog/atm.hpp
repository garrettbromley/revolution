class atm_menu {
	idd = 10000;
	movingEnable = false;
	enableSimulation = true;
	
	onKeyDown = "[_this,10003,'money'] call Menu_fnc_keydown;";
	onUnload = "keydownAmount = nil;atmPlayers = nil;";
	
	class controlsBackground {
		class backgroundBorder: IGUIBack
		{
			idc = -1;
			style = 192;
			x = 0.145875 * safezoneW + safezoneX;
			y = 0.15296298 * safezoneH + safezoneY;
			w = 0.70825 * safezoneW;
			h = 0.85407404 * safezoneH;
			colorBackground[] = {0,0,0,0.9};
		};
		class background: IGUIBack
		{
			idc = -1;
			style = 192;
			x = 0.15 * safezoneW + safezoneX;
			y = 0.16 * safezoneH + safezoneY;
			w = 0.7 * safezoneW;
			h = 0.84 * safezoneH;
			colorBackground[] = {0.15,0.15,0.15,0.9};
		};
		class body: IGUIBack
		{
			idc = -1;

			x = 0.2625 * safezoneW + safezoneX;
			y = 0.22 * safezoneH + safezoneY;
			w = 0.475 * safezoneW;
			h = 0.76 * safezoneH;
			colorBackground[] = {0.1,0.1,0.1,0.9};
		};
	}
	
	class controls {
		class close: RscButton
		{
			idc = -1;
			text = "X";
			x = 0.8125 * safezoneW + safezoneX;
			y = 0.18 * safezoneH + safezoneY;
			w = 0.025 * safezoneW;
			h = 0.04 * safezoneH;
			onButtonClick = "closeDialog 0;";
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		class welcomeTxt: RscText
		{
			idc = 10001;
			text = "Welcome to SUATMM! Never give your account credentials to anyone!";
			x = 0.275 * safezoneW + safezoneX;
			y = 0.24 * safezoneH + safezoneY;
			w = 0.45 * safezoneW;
			h = 0.04 * safezoneH;
			sizeEx = 0.03 * safezoneH;
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		class accountBalanceTxt: RscText
		{
			idc = -1;
			text = "ACCOUNT BALANCE";
			style = "0x02";
			font = "RobotoCondensedBold";
			x = 0.275 * safezoneW + safezoneX;
			y = 0.3 * safezoneH + safezoneY;
			w = 0.45 * safezoneW;
			h = 0.18 * safezoneH;
			sizeEx = 0.14 * safezoneH;
		};
		class accountBalance: RscText
		{
			idc = 10002;
			text = "$ XXX,XXX";
			style = "0x02";
			font = "RobotoCondensedBold";
			x = 0.275 * safezoneW + safezoneX;
			y = 0.48 * safezoneH + safezoneY;
			w = 0.45 * safezoneW;
			h = 0.1 * safezoneH;
			colorText[] = {0.131,0.736,0.131,1};
			sizeEx = 0.1 * safezoneH;
		};
		class amountBG: IGUIBack
		{
			idc = -1;
			x = 0.3625 * safezoneW + safezoneX;
			y = 0.6 * safezoneH + safezoneY;
			w = 0.275 * safezoneW;
			h = 0.08 * safezoneH;
			colorBackground[] = {0.282,0.282,0.282,1};
		};
		class amount: RscText
		{
			idc = 10003;
			text = "$ XXX,XXX";
			style = 1;
			x = 0.3625 * safezoneW + safezoneX;
			y = 0.6 * safezoneH + safezoneY;
			w = 0.208 * safezoneW;
			h = 0.08 * safezoneH;
			colorBackground[] = {0.282,0.282,0.282,0};
			sizeEx = 0.08 * safezoneH;
		};
		class player: RscCombo
		{
			idc = 10004;
			x = 0.425 * safezoneW + safezoneX;
			y = 0.72 * safezoneH + safezoneY;
			w = 0.15 * safezoneW;
			h = 0.04 * safezoneH;
			colorBackground[] = {1,1,1,0.2};
			sizeEx = 0.04 * safezoneH;
		};
		class transferBtn: RscButton
		{
			idc = -1;
			text = "Transfer";
			x = 0.6 * safezoneW + safezoneX;
			y = 0.72 * safezoneH + safezoneY;
			w = 0.125 * safezoneW;
			h = 0.04 * safezoneH;
			onButtonClick = "if(!isNil 'keydownAmount' && (lbCurSel ((findDisplay 10000) displayCtrl 10004)) != -1) then {[keydownAmount,atmPlayers select (lbCurSel ((findDisplay 10000) displayCtrl 10004))] call Atm_fnc_transfer;(findDisplay 10000) displayCtrl 10002 ctrlSetText format['$ %1',[bank] call Misc_fnc_formatNumber];};";
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		class depositBtn: RscButton
		{
			idc = -1;
			text = "Deposit";
			x = 0.6 * safezoneW + safezoneX;
			y = 0.92 * safezoneH + safezoneY;
			w = 0.125 * safezoneW;
			h = 0.04 * safezoneH;
			onButtonClick = "if(!isNil 'keydownAmount') then {[keydownAmount] call Atm_fnc_deposit;(findDisplay 10000) displayCtrl 10002 ctrlSetText format['$ %1',[bank] call Misc_fnc_formatNumber];};";
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		class withdrawBtn: RscButton
		{
			idc = -1;
			text = "Withdraw";
			x = 0.275 * safezoneW + safezoneX;
			y = 0.92 * safezoneH + safezoneY;
			w = 0.125 * safezoneW;
			h = 0.04 * safezoneH;
			onButtonClick = "if(!isNil 'keydownAmount') then {[keydownAmount] call Atm_fnc_withdraw;(findDisplay 10000) displayCtrl 10002 ctrlSetText format['$ %1',[bank] call Misc_fnc_formatNumber];};";
			colorBackground[] = {0.131,0.736,0.131,1};
		};
		
		
		//Arrows
		class RscButton_1600: RscButton
		{
			idc = -1;
			text = "->";
			x = 0.1625 * safezoneW + safezoneX;
			y = 0.92 * safezoneH + safezoneY;
			w = 0.0875 * safezoneW;
			h = 0.06 * safezoneH;
			sizeEx = 0.06 * safezoneH;
		};
		class RscButton_1601: RscButton
		{
			idc = -1;
			text = "->";
			x = 0.1625 * safezoneW + safezoneX;
			y = 0.84 * safezoneH + safezoneY;
			w = 0.0875 * safezoneW;
			h = 0.06 * safezoneH;
			sizeEx = 0.06 * safezoneH;
		};
		class RscButton_1602: RscButton
		{
			idc = -1;
			text = "->";
			x = 0.1625 * safezoneW + safezoneX;
			y = 0.76 * safezoneH + safezoneY;
			w = 0.0875 * safezoneW;
			h = 0.06 * safezoneH;
			sizeEx = 0.06 * safezoneH;
		};
		class RscButton_1603: RscButton
		{
			idc = -1;
			text = "->";
			x = 0.1625 * safezoneW + safezoneX;
			y = 0.68 * safezoneH + safezoneY;
			w = 0.0875 * safezoneW;
			h = 0.06 * safezoneH;
			sizeEx = 0.06 * safezoneH;
		};
		class RscButton_1609: RscButton
		{
			idc = -1;
			text = "->";
			x = 0.1625 * safezoneW + safezoneX;
			y = 0.6 * safezoneH + safezoneY;
			w = 0.0875 * safezoneW;
			h = 0.06 * safezoneH;
			sizeEx = 0.06 * safezoneH;
		};
		class RscButton_1604: RscButton
		{
			idc = -1;
			text = "<-";
			x = 0.75 * safezoneW + safezoneX;
			y = 0.92 * safezoneH + safezoneY;
			w = 0.0875 * safezoneW;
			h = 0.06 * safezoneH;
			sizeEx = 0.06 * safezoneH;
		};
		class RscButton_1605: RscButton
		{
			idc = -1;
			text = "<-";
			x = 0.75 * safezoneW + safezoneX;
			y = 0.84 * safezoneH + safezoneY;
			w = 0.0875 * safezoneW;
			h = 0.06 * safezoneH;
			sizeEx = 0.06 * safezoneH;
		};
		class RscButton_1606: RscButton
		{
			idc = -1;
			text = "<-";
			x = 0.75 * safezoneW + safezoneX;
			y = 0.76 * safezoneH + safezoneY;
			w = 0.0875 * safezoneW;
			h = 0.06 * safezoneH;
			sizeEx = 0.06 * safezoneH;
		};
		class RscButton_1607: RscButton
		{
			idc = -1;
			text = "<-";
			x = 0.75 * safezoneW + safezoneX;
			y = 0.68 * safezoneH + safezoneY;
			w = 0.0875 * safezoneW;
			h = 0.06 * safezoneH;
			sizeEx = 0.06 * safezoneH;
		};
		class RscButton_1608: RscButton
		{
			idc = -1;
			text = "<-";
			x = 0.75 * safezoneW + safezoneX;
			y = 0.6 * safezoneH + safezoneY;
			w = 0.0875 * safezoneW;
			h = 0.06 * safezoneH;
			sizeEx = 0.06 * safezoneH;
		};
	};
};