class CfgRemoteExec {
    // List of script functions allowed to be sent from client via remoteExec
    class Functions {
        mode = 2;   // State of remoteExec: 0-turned off, 1-turned on, taking whitelist into account, 2-turned on, however, ignoring whitelists (default because of backward compatibility)
        jip = 1;    // Ability to send jip messages: 0-disabled, 1-enabled (default)
        class Query_fnc_getPlayerData        { allowedTargets = 2; }; // can only target server
        class Query_fnc_getFines             { allowedTargets = 2; }; // can only target server
        class Query_fnc_insertPlayer         { allowedTargets = 2; }; // can only target server
        class Query_fnc_insertFine           { allowedTargets = 2; }; // can only target server
        class Query_fnc_insertPurchase       { allowedTargets = 2; }; // can only target server
        class Query_fnc_insertTransaction    { allowedTargets = 2; }; // can only target server
        class Query_fnc_checkPlayer          { allowedTargets = 2; }; // can only target server
        class Query_fnc_updateAllPlayerData  { allowedTargets = 2; }; // can only target server
        class Query_fnc_updatePlayerGear     { allowedTargets = 2; }; // can only target server
        class Query_fnc_updatePlayerRole     { allowedTargets = 2; }; // can only target server
        class Query_fnc_updatePlayerLicenses { allowedTargets = 2; }; // can only target server
        class Query_fnc_updatePlayerMoney    { allowedTargets = 2; }; // can only target server
        class Query_fnc_updatePlayerPosition { allowedTargets = 2; }; // can only target server
        class Query_fnc_updateFines          { allowedTargets = 2; }; // can only target server
        class Indicators_fnc_enableIndicator { allowedTargets = 1; }; // can only target clients
        class Indicators_fnc_disableIndicator{ allowedTargets = 1; }; // can only target clients
    };
};
