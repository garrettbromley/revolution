#define true 1
#define false 0

class CivGunShopConfig {
    class Weapons {
        // PDW2000
        class hgun_PDW2000_F {
            cost = 4000;
            requirements[] = {"gun_license"};
        };

        // Sting
        class SMG_02_F {
            cost = 4000;
            requirements[] = {"gun_license"};
        };

        // Vermin
        class SMG_01_F {
            cost = 5000;
            requirements[] = {"gun_license"};
        };

        // 4-five
        class hgun_Pistol_heavy_01_F {
            cost = 2500;
            requirements[] = {"gun_license"};
        };

        // ACP-C2
        class hgun_ACPC2_F {
            cost = 2500;
            requirements[] = {"gun_license"};
        };

        // PM 9mm
        class hgun_Pistol_01_F {
            cost = 1500;
            requirements[] = {"gun_license"};
        };

        // Rook-40
        class hgun_Rook40_F {
            cost = 2000;
            requirements[] = {"gun_license"};
        };

        // Zubr
        class hgun_Pistol_heavy_02_F {
            cost = 3000;
            requirements[] = {"gun_license"};
        };

        // Flare Gun
        class hgun_Pistol_Signal_F {
            cost = 1000;
            requirements[] = {};
        };
    };

    class Attachments {
        class optic_MRD {
            cost = 250;
        };

        class optic_Yorris {
            cost = 250;
        };

        class optic_ACO_grn_smg {
            cost = 400;
        };

        class optic_Aco_smg {
            cost = 400;
        };

        class optic_Holosight_smg {
            cost = 500;
        };

        class optic_Holosight_smg_blk_F {
            cost = 500;
        };

        class acc_flashlight {
            cost = 100;
        };
    };

    class Magazines {
        class 11Rnd_45ACP_Mag {
            cost = 10;
        };

        class 9Rnd_45ACP_Mag {
            cost = 10;
        };

        class 10Rnd_9x21_Mag {
            cost = 10;
        };

        class 16Rnd_9x21_Mag {
            cost = 10;
        };

        class 6Rnd_45ACP_Cylinder {
            cost = 10;
        };

        class 6Rnd_RedSignal_F {
            cost = 10;
        };

        class 6Rnd_GreenSignal_F {
            cost = 10;
        };

        class 30Rnd_9x21_Yellow_Mag {
            cost = 20;
        };

        class 30Rnd_9x21_Green_Mag {
            cost = 20;
        };

        class 30Rnd_9x21_Red_Mag {
            cost = 20;
        };

        class 30Rnd_9x21_Mag {
            cost = 20;
        };

        class 30Rnd_9x21_Mag_SMG_02 {
            cost = 20;
        };

        class 30Rnd_9x21_Mag_SMG_02_Tracer_Red {
            cost = 20;
        };

        class 30Rnd_9x21_Mag_SMG_02_Tracer_Yellow {
            cost = 20;
        };

        class 30Rnd_9x21_Mag_SMG_02_Tracer_Green {
            cost = 20;
        };

        class 30Rnd_45ACP_Mag_SMG_01 {
            cost = 20;
        };

        class 30Rnd_45ACP_Mag_SMG_01_Tracer_Green {
            cost = 20;
        };

        class 30Rnd_45ACP_Mag_SMG_01_Tracer_Red {
            cost = 20;
        };

        class 30Rnd_45ACP_Mag_SMG_01_Tracer_Yellow {
            cost = 20;
        };
    };
};

class CivClothingShopConfig {
    uniforms[] = {
        {"U_I_C_Soldier_Bandit_4_F",    100},
        {"U_I_C_Soldier_Bandit_1_F",    100},
        {"U_I_C_Soldier_Bandit_2_F",    100},
        {"U_I_C_Soldier_Bandit_5_F",    100},
        {"U_I_C_Soldier_Bandit_3_F",    100},
        {"U_C_Man_casual_2_F",          100},
        {"U_C_Man_casual_3_F",          100},
        {"U_C_Man_casual_1_F",          100},
        {"U_C_Poloshirt_blue",          100},
        {"U_C_Poloshirt_burgundy",      100},
        {"U_C_Poloshirt_redwhite",      100},
        {"U_C_Poloshirt_salmon",        100},
        {"U_C_Poloshirt_stripped",      100},
        {"U_C_Poloshirt_tricolour",     100},
        {"U_Competitor",                100},
        {"U_C_Driver_1_black",          100},
        {"U_C_Driver_1_blue",           100},
        {"U_C_Driver_2",                100},
        {"U_C_Driver_1",                100},
        {"U_C_Driver_1_green",          100},
        {"U_C_Driver_1_orange",         100},
        {"U_C_Driver_1_red",            100},
        {"U_C_Driver_3",                100},
        {"U_C_Driver_4",                100},
        {"U_C_Driver_1_white",          100},
        {"U_C_Driver_1_yellow",         100},
        {"U_BG_Guerilla2_2",            100},
        {"U_BG_Guerilla2_1",            100},
        {"U_BG_Guerilla2_3",            100},
        {"U_BG_Guerilla3_1",            100},
        {"U_C_HunterBody_grn",          100},
        {"U_OrestesBody",               100},
        {"U_C_Journalist",              100},
        {"U_Rangemaster",               100},
        {"U_C_man_sport_1_F",           100},
        {"U_C_man_sport_3_F",           100},
        {"U_C_man_sport_2_F",           100},
        {"U_C_Man_casual_6_F",          100},
        {"U_C_Man_casual_4_F",          100},
        {"U_C_Man_casual_5_F",          100},
        {"U_C_WorkerCoveralls",         100},
        {"U_C_Poor_1",                  100},
        {"U_I_G_resistanceLeader_F",    100},
        {"U_NikosAgedBody",             500},
        {"U_I_Wetsuit",                 200},
        {"U_O_Wetsuit",                 200},
        {"U_B_Wetsuit",                 200}
    };

    vests[] = {
        {"V_Rangemaster_belt",          200},
        {"V_RebreatherIA",              200},
        {"V_RebreatherIR",              200},
        {"V_RebreatherB",               200},
        {"V_BandollierB_blk",           200},
        {"V_BandollierB_cbr",           200},
        {"V_BandollierB_ghex_F",        200},
        {"V_BandollierB_rgr",           200},
        {"V_BandollierB_khk",           200},
        {"V_BandollierB_oli",           200},
        {"V_Press_F",                   200}
    };

    headgear[] = {
        {"H_Booniehat_khk",             50},
        {"H_Booniehat_oli",             50},
        {"H_Booniehat_indp",            50},
        {"H_Booniehat_mcamo",           50},
        {"H_Booniehat_grn",             50},
        {"H_Booniehat_tan",             50},
        {"H_Booniehat_dirty",           50},
        {"H_Booniehat_dgtl",            50},
        {"H_Booniehat_khk_hs",          50},
        {"H_Booniehat_tna_F",           50},
        {"H_Cap_red",                   50},
        {"H_Cap_blu",                   50},
        {"H_Cap_oli",                   50},
        {"H_Cap_headphones",            50},
        {"H_Cap_tan",                   50},
        {"H_Cap_blk",                   50},
        {"H_Cap_blk_CMMG",              50},
        {"H_Cap_brn_SPECOPS",           50},
        {"H_Cap_tan_specops_US",        50},
        {"H_Cap_khaki_specops_UK",      50},
        {"H_Cap_grn",                   50},
        {"H_Cap_grn_BI",                50},
        {"H_Cap_blk_Raven",             50},
        {"H_Cap_blk_ION",               50},
        {"H_Cap_oli_hs",                50},
        {"H_Cap_press",                 50},
        {"H_Cap_usblack",               50},
        {"H_Cap_surfer",                50},
        {"H_Cap_oli_Syndikat_F",        50},
        {"H_Cap_tan_Syndikat_F",        50},
        {"H_Cap_blk_Syndikat_F",        50},
        {"H_Cap_grn_Syndikat_F",        50},
        {"H_Bandanna_surfer",           50},
        {"H_Bandanna_khk",              50},
        {"H_Bandanna_khk_hs",           50},
        {"H_Bandanna_cbr",              50},
        {"H_Bandanna_sgg",              50},
        {"H_Bandanna_sand",             50},
        {"H_Bandanna_surfer_blk",       50},
        {"H_Bandanna_surfer_grn",       50},
        {"H_Bandanna_gry",              50},
        {"H_Bandanna_blu",              50},
        {"H_Bandanna_camo",             50},
        {"H_Bandanna_mcamo",            50},
        {"H_Beret_blk",                 50},
        {"H_Beret_red",                 50},
        {"H_Beret_grn",                 50},
        {"H_Watchcap_blk",              50},
        {"H_Watchcap_cbr",              50},
        {"H_Watchcap_khk",              50},
        {"H_Watchcap_camo",             50},
        {"H_Watchcap_sgg",              50},
        {"H_StrawHat",                  50},
        {"H_StrawHat_dark",             50},
        {"H_Hat_blue",                  50},
        {"H_Hat_brown",                 50},
        {"H_Hat_camo",                  50},
        {"H_Hat_grey",                  50},
        {"H_Hat_tan",                   50},
        {"H_RacingHelmet_1_F",          50},
        {"H_RacingHelmet_2_F",          50},
        {"H_RacingHelmet_3_F",          50},
        {"H_RacingHelmet_4_F",          50},
        {"H_RacingHelmet_1_black_F",    50},
        {"H_RacingHelmet_1_blue_F",     50},
        {"H_RacingHelmet_1_green_F",    50},
        {"H_RacingHelmet_1_red_F",      50},
        {"H_RacingHelmet_1_white_F",    50},
        {"H_RacingHelmet_1_yellow_F",   50},
        {"H_RacingHelmet_1_orange_F",   50},
        {"H_Cap_marshal",               50}
    };


    facewear[] = {
        {"G_Aviator",                   50},
        {"G_Bandanna_aviator",          50},
        {"G_Bandanna_beast",            50},
        {"G_Bandanna_blk",              50},
        {"G_Bandanna_khk",              50},
        {"G_Bandanna_oli",              50},
        {"G_Bandanna_shades",           50},
        {"G_Bandanna_sport",            50},
        {"G_Bandanna_tan",              50},
        {"G_Diving",                    50},
        {"G_I_Diving",                  50},
        {"G_O_Diving",                  50},
        {"G_B_Diving",                  50},
        {"G_Lady_Blue",                 50},
        {"G_Shades_Black",              50},
        {"G_Shades_Blue",               50},
        {"G_Shades_Green",              50},
        {"G_Shades_Red",                50},
        {"G_Spectacles",                50},
        {"G_Sport_Red",                 50},
        {"G_Sport_Blackyellow",         50},
        {"G_Sport_BlackWhite",          50},
        {"G_Sport_Checkered",           50},
        {"G_Sport_Blackred",            50},
        {"G_Sport_Greenblack",          50},
        {"G_Squares_Tinted",            50},
        {"G_Squares",                   50},
        {"G_Spectacles_Tinted",         50}
    };

    nvg[] = {
        {"NVGoggles_OPFOR",             50},
        {"NVGoggles",                   50},
        {"NVGoggles_INDEP",             50},
        {"NVGoggles_tna_F",             50}
    };

    equipment[] = {
        {"Chemlight_blue",              50},
        {"Chemlight_green",             50},
        {"Chemlight_yellow",            50},
        {"Chemlight_red",               50},
        {"Binocular",                   100},
        {"Rangefinder",                 500},
        {"ItemMap",                     10},
        {"ItemGPS",                     50},
        {"ItemCompass",                 5},
        {"ItemWatch",                   5}
    };

    backpacks[] = {
        // Name                        Price    Storage Space
        {"B_AssaultPack_blk",           100,        100},
        {"B_AssaultPack_cbr",           100,        100},
        {"B_AssaultPack_dgtl",          100,        100},
        {"B_AssaultPack_rgr",           100,        100},
        {"B_AssaultPack_ocamo",         100,        100},
        {"B_AssaultPack_khk",           100,        100},
        {"B_AssaultPack_mcamo",         100,        100},
        {"B_AssaultPack_sgg",           100,        100},
        {"B_AssaultPack_tna_F",         100,        100},
        {"B_Bergen_dgtl_F",             500,        500},
        {"B_Bergen_hex_F",              500,        500},
        {"B_Bergen_mcamo_F",            500,        500},
        {"B_Bergen_tna_F",              500,        500},
        {"B_Carryall_cbr",              400,        400},
        {"B_Carryall_ghex_F",           400,        400},
        {"B_Carryall_ocamo",            400,        400},
        {"B_Carryall_khk",              400,        400},
        {"B_Carryall_mcamo",            400,        400},
        {"B_Carryall_oli",              400,        400},
        {"B_Carryall_oucamo",           400,        400},
        {"B_FieldPack_blk",             100,        100},
        {"B_FieldPack_cbr",             100,        100},
        {"B_FieldPack_ghex_F",          100,        100},
        {"B_FieldPack_ocamo",           100,        100},
        {"B_FieldPack_khk",             100,        100},
        {"B_FieldPack_oli",             100,        100},
        {"B_FieldPack_oucamo",          100,        100},
        {"B_Kitbag_cbr",                150,        150},
        {"B_Kitbag_rgr",                150,        150},
        {"B_Kitbag_mcamo",              150,        150},
        {"B_Kitbag_sgg",                150,        150},
        {"B_Parachute",                 100,        0},
        {"B_TacticalPack_blk",          200,        200},
        {"B_TacticalPack_rgr",          200,        200},
        {"B_TacticalPack_ocamo",        200,        200},
        {"B_TacticalPack_mcamo",        200,        200},
        {"B_TacticalPack_oli",          200,        200},
        {"B_AssaultPack_Kerry",         100,        100},
        {"B_ViperHarness_blk_F",        300,        250},
        {"B_ViperHarness_ghex_F",       300,        250},
        {"B_ViperHarness_hex_F",        300,        250},
        {"B_ViperHarness_khk_F",        300,        250},
        {"B_ViperHarness_oli_F",        300,        250},
        {"B_ViperLightHarness_blk_F",   200,        200},
        {"B_ViperLightHarness_ghex_F",  200,        200},
        {"B_ViperLightHarness_hex_F",   200,        200},
        {"B_ViperLightHarness_khk_F",   200,        200},
        {"B_ViperLightHarness_oli_F",   200,        200}
    };
};
