class SpeedCameras {

    //Define the possible locations for your speed cameras.
    //position -> Get the position via: diag_log(getPosATL player) -> replace the [] with {}
    //direction -> Get the position via: diag_log((getDir player) - 90)
    //speedlimit -> Choose any value you like. In a zone where people should drive 50, make set it to 55 or 60, to catch those speeders!
	SpeedCameras[] = {
	  //{{<position>}, <direction>, <speedlimit>},
        {{5788.3,10516.2,0}, 267.65, 40},   // Georgetown Main Road
        {{5025.18,9740.22,0}, 132.86, 65},  // Outside Georgetown (SE Road)
        {{5773.08,11224,0}, 199.575, 65}    // Outside Georgetown (NW Road)
        //{{9626.39,13550.1,0}, 17.9351, 40}, // La Rochelle Main Road
        //{{11604.7,3051.27,0}, 214.871, 65}, // Lijnhaven by Airport
        //{{11639.6,2638.09,0}, 137.468, 40}, // Lijnhaven Main Road
        //{{5561.5,4137.7,0}, 134.437, 40},   // Katkoula Main Road
        //{{4784.79,5122.56,0}, 220.876, 65}, // Big Bridge
        //{{11377.7,5296.88,0}, 265.188, 65}, // Big Bridge
        //{{8612.13,10075.9,0}, 130.624, 65}, // Tanouka (S Road)
        //{{9017.08,10369.7,0}, -86.8926, 65} // Tanouka (N Road)
	};

    //Shall the user get its driver licenses revoked? (Truck and Car license)
    RemoveDriverLicense    = true;
    RemoveTruckLicense     = true;

    //Shall the player be added to the wanted list with a speeding entry?
    AddPlayerToWanted      = true;
	WantedCase			   = "4";     //The number/numbers+charaters that describe the crime you want the player to be added for

    //Let the player pay a fine from his bank account
    SpeedingFee            = 5000;    // 0 = No fine payed | Any value over 0 $ will be then subtracted from the bank account

    //Define the messages, which will be shown to the player, when he was driving too fast
	InfoMSG                = "You have been driving %1 km/h too fast!";
	RemoveDriverLicenseMSG = "Your drivers license has been removed!";
	RemoveTruckLicenseMSG  = "Your truck license has been removed!";
	AddPlayerToWantedMSG   = "A wanted record about you has been created!";
	SpeedingFeeMSG         = "The police are fining you for $%1 for speeding!<br/>Report to the nearest police station to pay your fee. ";
};
