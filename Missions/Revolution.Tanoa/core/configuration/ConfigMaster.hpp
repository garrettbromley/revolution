#define true 1
#define false 0

class Master_Settings {
    // Starting Money
    startingCash                    = 500000;       // Starting Cash Amount
    startingBank                    = 500000;       // Starting Bank Account
    startingCreditLimit             = 1000;         // Starting Credit Limit

    // Bank Parameters
    enableWait = true;          // Enable waiting for bank transfers?
    waitPeriod = 5;             // How long to wait for bank transfer (minutes)

    // Play Area Restriction
    timeToReturn = 30;          // Seconds to return to the play area

    // Default Civ Uniforms and spawn location/Direction
    startingUniforms                = ["U_C_Man_casual_6_F", "U_C_Man_casual_4_F", "U_C_Man_casual_5_F"];   // Tanoa Summer Outfits
    startingPos                     = [5590.382,10423.88,7.608];    // On the docks
    startingDir                     = 128;                        // Facing the city

    // Default Gear
    defaultCopGear                  = "[[],[],[""hgun_P07_F"",""muzzle_snds_L"","""","""",[""16Rnd_9x21_Mag"",16],[],""""],[""U_B_GEN_Soldier_F"",[[""16Rnd_9x21_Mag"",4,16]]],[""V_TacVest_gen_F"",[]],[],""H_MilCap_gen_F"","""",[],[""ItemMap"",""ItemGPS"","""",""ItemCompass"",""ItemWatch"",""""]]";
    defaultMedGear                  = "[[],[],[],[""U_C_Scientist"",[]],[],[""B_FieldPack_oucamo"",[]],""H_Cap_marshal"","""",[],[""ItemMap"",""ItemGPS"","""",""ItemCompass"",""ItemWatch"",""""]]";
};

#include "ConfigCivShop.hpp"
#include "ConfigCopShop.hpp"
#include "ConfigLicenses.hpp"
#include "ConfigIndicators.hpp"
#include "ConfigSpeedCameras.hpp"
