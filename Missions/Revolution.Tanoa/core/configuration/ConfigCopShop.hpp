#define true 1
#define false 0

class CopGunShopConfig {
    class Weapons {
        // P0-7 Tazer
        class hgun_P07_F {
            cost = 1000;
            minLevel = 1;
        };

        // SDAR Tazer Rifle
        class arifle_SDAR_F {
            cost = 2000;
            minLevel = 1;
        };

        // Protector 9MM
        class SMG_05_F {
            cost = 4000;
            minLevel = 2;
        };

        // SPAR 5.56
        class arifle_SPAR_01_blk_F {
            cost = 7500;
            minLevel = 3;
        };

        // SPAR 7.62
        class arifle_SPAR_03_blk_F {
            cost = 10000;
            minLevel = 4;
        };

        // MAR-10
        class srifle_DMR_02_F {
            cost = 15000;
            minLevel = 5;
        };
    };

    class Attachments {
        class optic_ACO_grn {
        	cost = 500;
        	minLevel = 1;
        };

        class optic_Aco {
        	cost = 500;
        	minLevel = 1;
        };

        class optic_ACO_grn_smg {
        	cost = 500;
        	minLevel = 1;
        };

        class optic_Aco_smg {
        	cost = 500;
        	minLevel = 1;
        };

        class optic_AMS {
        	cost = 500;
        	minLevel = 5;
        };

        class optic_ERCO_blk_F {
        	cost = 500;
        	minLevel = 1;
        };

        class optic_Holosight_blk_F {
        	cost = 500;
        	minLevel = 1;
        };

        class optic_Holosight_smg_blk_F {
        	cost = 500;
        	minLevel = 1;
        };

        class optic_SOS {
        	cost = 500;
        	minLevel = 5;
        };

        class optic_MRCO {
        	cost = 500;
        	minLevel = 4;
        };

        class optic_Hamr {
        	cost = 500;
        	minLevel = 1;
        };

        class acc_flashlight {
        	cost = 500;
        	minLevel = 1;
        };

        class acc_pointer_IR {
        	cost = 500;
        	minLevel = 4;
        };

        class bipod_01_F_blk {
        	cost = 500;
        	minLevel = 3;
        };
    };

    class Magazines {
        class 16Rnd_9x21_Mag {
        	cost = 50;
        };

        class 30Rnd_9x21_Mag_SMG_02 {
        	cost = 50;
        };

        class 30Rnd_9x21_Mag_SMG_02_Tracer_Red {
        	cost = 50;
        };

        class 30Rnd_9x21_Mag_SMG_02_Tracer_Yellow {
        	cost = 50;
        };

        class 30Rnd_9x21_Mag_SMG_02_Tracer_Green {
        	cost = 50;
        };

        class 20Rnd_556x45_UW_mag {
        	cost = 50;
        };

        class 30Rnd_556x45_Stanag {
        	cost = 50;
        };

        class 30Rnd_556x45_Stanag_green {
        	cost = 50;
        };

        class 30Rnd_556x45_Stanag_red {
        	cost = 50;
        };

        class 30Rnd_556x45_Stanag_Tracer_Red {
        	cost = 50;
        };

        class 30Rnd_556x45_Stanag_Tracer_Green {
        	cost = 50;
        };

        class 30Rnd_556x45_Stanag_Tracer_Yellow {
        	cost = 50;
        };

        class 20Rnd_762x51_Mag {
        	cost = 50;
        };

        class 10Rnd_338_Mag {
        	cost = 50;
        };
    };
};

class CopClothingShopConfig {
    uniforms[] = {
        // Name                        Price      Level
        {"U_B_GEN_Soldier_F",           100,        1},
        {"U_B_GEN_Commander_F",         200,        3},
        {"U_I_G_Story_Protagonist_F",   500,        4},
        {"U_Marshal",                   500,        5}
    };

    vests[] = {
        // Name                        Price      Level
        {"V_Rangemaster_belt",          100,        1},
        {"V_TacVest_blk_POLICE",        200,        1},
        {"V_TacVest_gen_F",             200,        1},
        {"V_PlateCarrier2_blk",         1000,       3},
        {"V_PlateCarrierGL_blk",        2500,       5}
    };

    headgear[] = {
        // Name                        Price      Level
        {"H_Watchcap_blk",              50,         1},
        {"H_MilCap_gen_F",              50,         1},
        {"H_HelmetSpecB_blk",           250,        3},
        {"H_Beret_gen_F",               100,        5}
    };


    facewear[] = {
        // Name                        Price      Level
        {"G_Aviator",                   500,        1},
        {"G_Shades_Black",              50,         1},
        {"G_Shades_Blue",               50,         1},
        {"G_Shades_Green",              50,         1},
        {"G_Shades_Red",                50,         1},
        {"G_Spectacles",                50,         1},
        {"G_Sport_Red",                 50,         1},
        {"G_Sport_Blackyellow",         50,         1},
        {"G_Sport_BlackWhite",          50,         1},
        {"G_Sport_Checkered",           50,         1},
        {"G_Sport_Blackred",            50,         1},
        {"G_Sport_Greenblack",          50,         1},
        {"G_Squares_Tinted",            50,         1},
        {"G_Squares",                   50,         1},
        {"G_Spectacles_Tinted",         50,         1},
        {"G_Combat",                    100,        3},
        {"G_Balaclava_TI_blk_F",        500,        5},
        {"G_Balaclava_TI_G_blk_F",      700,        5}
    };

    nvg[] = {
        // Name                        Price      Level
        {"NVGoggles_OPFOR",             50,         1},
        {"NVGogglesB_blk_F",            500,        5}
    };

    equipment[] = {
        // Name                        Price      Level
        {"Chemlight_blue",              50,         1},
        {"Chemlight_green",             50,         1},
        {"Chemlight_yellow",            50,         1},
        {"Chemlight_red",               50,         1},
        {"Binocular",                   100,        1},
        {"Rangefinder",                 500,        1},
        {"ItemMap",                     10,         1},
        {"ItemGPS",                     50,         1},
        {"ItemCompass",                 5,          1},
        {"ItemWatch",                   5,          1}
    };

    backpacks[] = {
        // Name                        Price    Storage Space      Level
        {"B_AssaultPack_blk",           100,        100,            1},
        {"B_FieldPack_blk",             100,        100,            1},
        {"B_TacticalPack_blk",          250,        200,            2},
        {"B_ViperLightHarness_blk_F",   500,        250,            3},
        {"B_ViperHarness_blk_F",        1000,       300,            4}
    };
};
