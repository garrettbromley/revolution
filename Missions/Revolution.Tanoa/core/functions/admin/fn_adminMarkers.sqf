/*
	File: fn_adminMarkers.sqf 
	Author: Garrett 
	Description:
		Turns the admin markers on
*/

private _markers = [];
private _cops = [];
private _civs = [];

[] call admin_fnc_removeActions;

adminMarkers = true;
while {adminMarkers} do {
	waitUntil {visibleMap};

	{
		if (_x getVariable ["isCop", false]) then { _cops pushBack _x; } else { _civs pushBack _x; };
	} forEach allUnits; //Fetch list of units

	// Create markers
	{
		_marker = createMarkerLocal [format ["%1_marker", _x], getPos _x];
		_marker setMarkerColorLocal "ColorBlue";
		_marker setMarkerTypeLocal "Mil_dot";
		_marker setMarkerTextLocal format ["%1", name _x];
		_markers set [count _markers, [_marker,_x]];
	} forEach _cops;
	{
		_marker = createMarkerLocal [format ["%1_marker", _x], getPos _x];
		_marker setMarkerColorLocal "ColorWhite";
		_marker setMarkerTypeLocal "Mil_dot";
		_marker setMarkerTextLocal format ["%1", name _x];
		_markers set [count _markers, [_marker,_x]];
	} forEach _civs;

	while {visibleMap} do {
		{
			private ["_marker", "_unit"];
			_marker = _x select 0;
			_unit = _x select 1;
			if (!isNil "_unit" && {!isNull _unit}) then {
				_marker setMarkerPosLocal (getPos _unit);
			};
		} forEach _markers;
		if (!visibleMap) exitWith {};
		sleep 0.02;
	};

	{ deleteMarkerLocal (_x select 0); } forEach _markers;
	_markers = [];
	_cops = [];
	_civs = [];
};