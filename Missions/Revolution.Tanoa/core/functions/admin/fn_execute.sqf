/*
	File: fn_execute.sqf 
	Author: Garrett 
	Description:
		Executes the admin tools menu
*/

private ["_ID"];

adminToolsOpen = true;
adminToolActions = [];

// Add actions
if (adminLevel == 1) then {
	_ID = player addaction [("<t color=""#0074E8"">" + ("Admin Markers On") +"</t>"),{ [] spawn admin_fnc_adminMarkers; }, "",5,false,false,"", "!adminMarkers && adminToolsOpen"]; adminToolActions pushBack _ID;
	_ID = player addaction [("<t color=""#0074E8"">" + ("Admin Markers Off") +"</t>"),{ [] spawn admin_fnc_adminMarkersOff; }, "",5,false,false,"", "adminMarkers && adminToolsOpen"]; adminToolActions pushBack _ID;
	_ID = player addaction [("<t color=""#0074E8"">" + ("Teleport") +"</t>"),{ [] spawn admin_fnc_teleport; }, "",5,false,false,"", "adminToolsOpen"]; adminToolActions pushBack _ID;
	_ID = player addaction [("<t color=""#0074E8"">" + ("Heal Yourself") +"</t>"),{ [] spawn admin_fnc_healSelf; }, "",5,false,false,"", "adminToolsOpen"]; adminToolActions pushBack _ID;
	_ID = player addaction [("<t color=""#0074E8"">" + ("Repair Buildings") +"</t>"),{ [] spawn admin_fnc_repairBuild; }, "",5,false,false,"", "adminToolsOpen"]; adminToolActions pushBack _ID;
	_ID = player addaction [("<t color=""#0074E8"">" + ("Repair Vehicle") +"</t>"),{ [] spawn admin_fnc_vehRepair; }, "",5,false,false,"", "adminToolsOpen"]; adminToolActions pushBack _ID;
	_ID = player addaction [("<t color=""#0074E8"">" + ("Delete Vehicle") +"</t>"),{ [] spawn admin_fnc_vehDelete; }, "",5,false,false,"", "adminToolsOpen"]; adminToolActions pushBack _ID;
};

if (adminLevel > 1) then {
	_ID = player addaction [("<t color=""#0074E8"">" + ("God Mode On") +"</t>"),{ [] spawn admin_fnc_godOn; }, "",5,false,false,"", "!adminGod && adminToolsOpen"]; adminToolActions pushBack _ID;
	_ID = player addaction [("<t color=""#0074E8"">" + ("God Mode Off") +"</t>"),{ [] spawn admin_fnc_godOff; }, "",5,false,false,"", "adminGod && adminToolsOpen"]; adminToolActions pushBack _ID;
	_ID = player addaction [("<t color=""#0074E8"">" + ("Vehicle God Mode On") +"</t>"),{ [] spawn admin_fnc_vehGodOn; }, "",5,false,false,"", "adminToolsOpen"]; adminToolActions pushBack _ID;
	_ID = player addaction [("<t color=""#0074E8"">" + ("Vehicle God Mode Off") +"</t>"),{ [] spawn admin_fnc_vehGodOff; }, "",5,false,false,"", "adminToolsOpen"]; adminToolActions pushBack _ID;
};