/*
	File: fn_repairbuild.sqf 
	Author: Garrett 
	Description:
		Repairs all static buildings in a 2 km radius of player
*/

private _buildingarray = nearestObjects [(position player), ["Static"], 2000];
{
    _x setDamage 0;
} forEach _buildingarray;

[] call admin_fnc_removeActions;