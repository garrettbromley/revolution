/*
	File: fn_init.sqf 
	Author: Garrett 
	Description:
		Initializes the admin tools
*/

waituntil { !alive player; !isnull (finddisplay 46); !isNil "adminLevel"; };
if (adminLevel > 0) then {
	adminToolsOpen = false;
	adminMarkers = false;
	adminGod = false;
	player addaction [("<t color=""#0074E8"">" + ("Tools Menu") +"</t>"),{ [] spawn admin_fnc_execute; }, "",5,false,false,"", "!adminToolsOpen"];
};