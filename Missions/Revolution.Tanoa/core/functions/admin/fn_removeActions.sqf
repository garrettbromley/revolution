/*
	File: fn_removeActions.sqf 
	Author: Garrett 
	Description:
		Removes the actions on the tools menu
*/

private _i = 0;

while {_i < count adminToolActions} do {
	player removeAction (adminToolActions select _i);
	_i = _i + 1;
};

adminToolActions = [];
adminToolsOpen = false;