/*
	File: fn_vehGodOn.sqf 
	Author: Garrett 
	Description:
		Gives a vehicle god mode
*/

if (vehicle player == player) then {
  cutText ["Put your cursor over the vehicle you wish to give God Mode.", "PLAIN"];

  private _timeForGod = 3;
  hint format ["Please wait %1 seconds for vehicle to be given God Mode.", _timeForGod];
  sleep _timeForGod;

  cursorTarget allowDamage false;
  cursorTarget addeventhandler ["fired", {(_this select 0) setvehicleammo 1}];
  hint "Vehicle has been given god mode.";
} else {
  vehicle player allowDamage false;
  adminVehicleGodEH = vehicle player addeventhandler ["fired", {(_this select 0) setvehicleammo 1}];
  hint "Vehicle has been given god mode.";
};

[] call admin_fnc_removeActions;