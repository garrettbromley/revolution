/*
	File: fn_healSelf.sqf 
	Author: Garrett 
	Description:
		Heals yourself
*/

hint format ["Healing..."];
player setDamage 0;

[] call admin_fnc_removeActions;