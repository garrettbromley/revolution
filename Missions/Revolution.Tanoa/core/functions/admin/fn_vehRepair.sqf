/*
	File: fn_vehRepair.sqf 
	Author: Garrett 
	Description:
		Repairs and flips vehicle
*/

if (vehicle player == player) then {
  	cutText ["Put your cursor over the vehicle you wish to repair.", "PLAIN"];

  	private _timeForRepair = 3;
  	hint format ["Please wait %1 seconds for vehicle to be repaired.", _timeForRepair];
	sleep _timeForRepair;

  	cutText ["Repair activated.", "PLAIN"];
	private _veh = cursorTarget;
	_veh setFuel 1;
	_veh setDamage 0;
	_veh setVehicleAmmo 1;
	_veh setpos [getpos _veh select 0, getpos _veh select 1, 0.5];
} else {
	cutText ["Repair activated.", "PLAIN"];
	private _veh = (vehicle player);
	_veh setFuel 1;
	_veh setDamage 0;
	_veh setVehicleAmmo 1;
	_veh setpos [getpos _veh select 0, getpos _veh select 1, 0.5];
};

[] call admin_fnc_removeActions;