/*
	File: fn_vehGodOff.sqf 
	Author: Garrett 
	Description:
		Turns off a vehicle's god mode
*/

if (vehicle player == player) then {
  cutText ["Put your cursor over the vehicle you wish to remove God Mode.", "PLAIN"];

  private _timeForGod = 3;
  hint format ["Please wait %1 seconds for vehicle to remove God Mode.", _timeForGod];
  sleep _timeForGod;

  cursorTarget allowDamage true;
  cursorTarget removeEventHandler ["fired", adminVehicleGodEH];
  hint "Vehicle god mode has been removed.";
} else {
  vehicle player allowDamage true;
  vehicle player removeEventHandler ["fired", adminVehicleGodEH];
  hint "Vehicle god mode has been removed.";
};

[] call admin_fnc_removeActions;
