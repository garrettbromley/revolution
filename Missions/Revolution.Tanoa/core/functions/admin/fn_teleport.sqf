/*
	File: fn_teleport.sqf 
	Author: Garrett 
	Description:
		Allows you to teleport anywhere
*/

cutText ["Click somewhere on the map to move there", "PLAIN"];
openMap [true, false];
onMapSingleClick "vehicle player setPos _pos; onMapSingleClick '';true; openMap [false, false];";

[] call admin_fnc_removeActions;