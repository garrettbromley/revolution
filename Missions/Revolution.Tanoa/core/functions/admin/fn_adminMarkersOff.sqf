/*
	File: fn_adminMarkersOff.sqf 
	Author: Garrett 
	Description:
		Turns the admin markers off
*/

adminMarkers = false;

[] call admin_fnc_removeActions;