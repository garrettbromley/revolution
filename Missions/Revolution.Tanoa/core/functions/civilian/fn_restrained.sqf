/*
    File: fn_restrained.sqf
    Author: Garrett
    Description:
        Restrains the person
*/

private ["_cop","_player","_vehicle"];
_cop = [_this,0,objNull,[objNull]] call BIS_fnc_param;
_player = player;
_vehicle = vehicle player;
if (isNull _cop) exitWith {};

// Monitor excessive restrainment
[] spawn {
    private ["_time", "_copsNear"];
    for "_i" from 0 to 1 step 0 do {
        _time = time;
        waitUntil {(time - _time) > (5 * 60)};

        if (!(player getVariable ["Restrained",false])) exitWith {};
        _copsNear = {!(_x isEqualTo player) && _x getVariable ["isCop", false] && alive _x && (getPos player) distance _x < 30} count playableUnits > 0;
        if (!(_copsNear) && (player getVariable ["Restrained",false]) && isNull objectParent player) exitWith {
            player setVariable ["Restrained", false, true];
            player setVariable ["Escorting", false, true];
            detach player;
            titleText["You have been released automatically for excessive restrainment time","PLAIN"];
        };
    };
};

titleText[format ["You have been restrained by %1", name _cop], "PLAIN"];
[player] remoteExecCall ["misc_fnc_handcuffSound", -2];
disable_getIn = true;
disable_getOut = false;

while {player getVariable  "Restrained"} do {
    if (isNull objectParent player) then {
        player playMove "AmovPercMstpSnonWnonDnon_Ease";
    };

    _state = vehicle player;
    waitUntil {animationState player != "AmovPercMstpSnonWnonDnon_Ease" || !(player getVariable "Restrained") || vehicle player != _state};

    // Player dies :(
    if (!alive player) exitWith {
        player setVariable ["Restrained",false,true];
        player setVariable ["Escorting",false,true];
        detach _player;
    };

    // Cop dies :(
    if (!alive _cop) then {
        player setVariable ["Escorting", false, true];
        detach player;
    };

    // Player gets in vehicle (bad boy)
    if (!(isNull objectParent player) && disable_getIn) then {
        player action["eject", vehicle player];
    };

    // Assign vehicle to player?
    if (!(isNull objectParent player) && !(vehicle player isEqualTo _vehicle)) then {
        _vehicle = vehicle player;
    };

    // Player exits vehicle (bad boy)
    if (isNull objectParent player && disable_getOut) then {
        player moveInCargo _vehicle;
    };

    // Player Moves to driver seat (bad boy)
    if (!(isNull objectParent player) && disable_getOut && (driver (vehicle player) isEqualTo player)) then {
        player action["eject", vehicle player];
        player moveInCargo _vehicle;
    };

    // Player moves to turret (super bad boy)
    if (!(isNull objectParent player) && disable_getOut) then {
        _turrets = [[-1]] + allTurrets _vehicle;
        {
            if (_vehicle turretUnit [_x select 0] isEqualTo player) then {
                player action["eject",vehicle player];
                sleep 1;
                player moveInCargo _vehicle;
            };
        } forEach _turrets;
    };
};

// Once they are unrestrained
if (alive player) then {
    player switchMove "AmovPercMstpSlowWrflDnon_SaluteIn";
    player setVariable ["Escorting", false, true];
    detach player;
};
