/*
    File: fn_confirmPurchase.sqf
    Author: Garrett
    Description:
        Asks the user to confirm their purchase
        Takes their money
        Updates their gear and money in the database

        Need to spawn this, because using call crashes the game
*/

private ["_decision"];
params ["_message", "_total", "_oldGear", "_shopName"];

// Ask if they want to purchase
_message = [_message, "\n", "<br />", 10, false] call misc_fnc_stringReplace;


_decision = [_message, "Shopping Cart", "PURCHASE", "CANCEL"] call BIS_fnc_guiMessage;

// Process their decision
if (_decision) then {
    // Take their money
    if ((cash - _total) < 0) then {
        // Revert them to their previous loadout
        player setUnitLoadout _oldGear;
        hint format ["Not enough money for your purchase!\nMissing $%1", ((cash - _total) * -1)];
    } else {
        // Take money
        cash = cash - _total;

        // Save their money and gear in the database
        [] call update_fnc_money;
        [] call update_fnc_gear;

        // Add the purchase to the database
        [_shopName, _total] call shop_fnc_addPurchase;
    };
} else {
    // Revert them to their previous loadout
    player setUnitLoadout _oldGear;
};

[] call shop_fnc_flushArsenal;
