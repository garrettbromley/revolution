/*
    File: fn_addPurchase.sqf
    Author: Garrett
    Description:
        Tells the server to insert the player's purchase in the database
*/

params ["_shop", "_amount"];

[getPlayerUID player, _shop, _amount] remoteExec ["Query_fnc_insertPurchase", 2];
