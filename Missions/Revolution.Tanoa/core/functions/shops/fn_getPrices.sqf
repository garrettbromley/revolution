/*
    File: fn_getPrices.sqf
    Author: Garrett
    Description:
        Compiles a list of all the items and prices they will encounter
*/

private ["_returnData", "_allWeapons", "_allMags", "_allAttachments", "_uniformData", "_vestData", "_headgearData", "_facewearData", "_nvgData", "_equipmentData", "_allItems", "_backpackData"];

params ["_shopGunID", "_shopClothingID"];

_returnData = [];

// Compile list of weapons
_allWeapons =  "true" configClasses (MissionConfigFile >> "MissionCFGMain" >> _shopGunID >> "Weapons");
{
    private ["_cost"];
    _cost = (MissionConfigFile >> "MissionCFGMain" >> _shopGunID >> "Weapons" >> (configName _x) >> "cost") call BIS_fnc_getCfgData;
    _returnData pushback [configName _x, _cost];
} forEach _allWeapons;

// Compile list of magazines
_allMags =  "true" configClasses (MissionConfigFile >> "MissionCFGMain" >> _shopGunID >> "Magazines");
{
    private ["_cost"];
    _cost = (MissionConfigFile >> "MissionCFGMain" >> _shopGunID >> "Magazines" >> (configName _x) >> "cost") call BIS_fnc_getCfgData;
    _returnData pushback [configName _x, _cost];
} forEach _allMags;

// Compile list of items
_allAttachments =  "true" configClasses (MissionConfigFile >> "MissionCFGMain" >> _shopGunID >> "Attachments");
{
    private ["_cost"];
    _cost = (MissionConfigFile >> "MissionCFGMain" >> _shopGunID >> "Attachments" >> (configName _x) >> "cost") call BIS_fnc_getCfgData;
    _returnData pushback [configName _x, _cost];
} forEach _allAttachments;

_uniformData = (MissionConfigFile >> "MissionCFGMain" >> _shopClothingID >> "uniforms") call BIS_fnc_getCfgData;
_vestData = (MissionConfigFile >> "MissionCFGMain" >> _shopClothingID >> "vests") call BIS_fnc_getCfgData;
_headgearData = (MissionConfigFile >> "MissionCFGMain" >> _shopClothingID >> "headgear") call BIS_fnc_getCfgData;
_facewearData = (MissionConfigFile >> "MissionCFGMain" >> _shopClothingID >> "facewear") call BIS_fnc_getCfgData;
_nvgData = (MissionConfigFile >> "MissionCFGMain" >> _shopClothingID >> "nvg") call BIS_fnc_getCfgData;
_equipmentData = (MissionConfigFile >> "MissionCFGMain" >> _shopClothingID >> "equipment") call BIS_fnc_getCfgData;

_allItems = _uniformData + _vestData + _headgearData + _facewearData + _nvgData + _equipmentData;
{
    private ["_classname", "_cost"];
    _classname = _x select 0;
    _cost = _x select 1;
    _returnData pushback [_classname, _cost];
} forEach _allItems;

// Compile list of backpacks
_backpackData = (MissionConfigFile >> "MissionCFGMain" >> _shopClothingID >> "backpacks") call BIS_fnc_getCfgData;
{
    private ["_classname", "_cost"];
    _classname = _x select 0;
    _cost = _x select 1;
    _returnData pushback [_classname, _cost];
} forEach _backpackData;

_returnData;
