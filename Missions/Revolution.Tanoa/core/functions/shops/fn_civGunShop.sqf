/*
    File: fn_civGunShop.sqf
    Author: Garrett
    Description:
        Return the arsenal information to the caller
*/

private ["_result", "_items", "_backpacks", "_weapons", "_magazines", "_allWeapons", "_allMags", "_allAttachments"];

// Compile list of weapons
_weapons = [];
_allWeapons =  "true" configClasses (MissionConfigFile >> "MissionCFGMain" >> "CivGunShopConfig" >> "Weapons");
{
    _weapons pushback (configName _x);
} forEach _allWeapons;

// Compile list of magazines
_magazines = [];
_allMags =  "true" configClasses (MissionConfigFile >> "MissionCFGMain" >> "CivGunShopConfig" >> "Magazines");
{
    _magazines pushback (configName _x);
} forEach _allMags;

// Compile list of items
_items = [];
_allAttachments =  "true" configClasses (MissionConfigFile >> "MissionCFGMain" >> "CivGunShopConfig" >> "Attachments");
{
    _items pushback (configName _x);
} forEach _allAttachments;

// Compile list of backpacks
_backpacks = [];

_result = [_weapons, _magazines, _items, _backpacks];

_result;
