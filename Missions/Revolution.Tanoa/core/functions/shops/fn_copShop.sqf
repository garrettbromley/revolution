/*
    File: fn_civCopShop.sqf
    Author: Garrett
    Description:
        Return the arsenal information to the caller
*/

private ["_result", "_items", "_backpacks", "_backpackData", "_weapons", "_magazines", "_allWeapons", "_allMags", "_allAttachments", "_allItems", "_uniformData", "_vestData", "_headgearData", "_facewearData", "_nvgData", "_equipmentData", "_cfgData"];

// Compile list of weapons
_weapons = [];
_allWeapons =  "true" configClasses (MissionConfigFile >> "MissionCFGMain" >> "CopGunShopConfig" >> "Weapons");
{
    private ["_levelRequired"];
    _levelRequired = (MissionConfigFile >> "MissionCFGMain" >> "CopGunShopConfig" >> "Weapons" >> (configName _x) >> "minLevel") call BIS_fnc_getCfgData;
    if (_levelRequired <= copLevel) then {
        _weapons pushback (configName _x);
    };
} forEach _allWeapons;

// Compile list of magazines
_magazines = [];
_allMags =  "true" configClasses (MissionConfigFile >> "MissionCFGMain" >> "CopGunShopConfig" >> "Magazines");
{
    _magazines pushback (configName _x);
} forEach _allMags;

// Compile list of items
_items = [];
_allAttachments =  "true" configClasses (MissionConfigFile >> "MissionCFGMain" >> "CopGunShopConfig" >> "Attachments");
{
    private ["_levelRequired"];
    _levelRequired = (MissionConfigFile >> "MissionCFGMain" >> "CopGunShopConfig" >> "Attachments" >> (configName _x) >> "minLevel") call BIS_fnc_getCfgData;
    if (_levelRequired <= copLevel) then {
        _items pushback (configName _x);
    };
} forEach _allAttachments;

_uniformData = (MissionConfigFile >> "MissionCFGMain" >> "CopClothingShopConfig" >> "uniforms") call BIS_fnc_getCfgData;
_vestData = (MissionConfigFile >> "MissionCFGMain" >> "CopClothingShopConfig" >> "vests") call BIS_fnc_getCfgData;
_headgearData = (MissionConfigFile >> "MissionCFGMain" >> "CopClothingShopConfig" >> "headgear") call BIS_fnc_getCfgData;
_facewearData = (MissionConfigFile >> "MissionCFGMain" >> "CopClothingShopConfig" >> "facewear") call BIS_fnc_getCfgData;
_nvgData = (MissionConfigFile >> "MissionCFGMain" >> "CopClothingShopConfig" >> "nvg") call BIS_fnc_getCfgData;
_equipmentData = (MissionConfigFile >> "MissionCFGMain" >> "CopClothingShopConfig" >> "equipment") call BIS_fnc_getCfgData;

_allItems = _uniformData + _vestData + _headgearData + _facewearData + _nvgData + _equipmentData;
{
    private ["_classname", "_levelRequired"];
    _classname = _x select 0;
    _levelRequired = _x select 2;
    if (_levelRequired <= copLevel) then {
        _items pushBack _classname;
    };
} forEach _allItems;

// Compile list of backpacks
_backpackData = (MissionConfigFile >> "MissionCFGMain" >> "CopClothingShopConfig" >> "backpacks") call BIS_fnc_getCfgData;

_backpacks = [];
{
    private ["_classname", "_levelRequired"];
    _classname = _x select 0;
    _levelRequired = _x select 3;
    if (_levelRequired <= copLevel) then {
        _backpacks pushBack _classname;
    };
} forEach _backpackData;

_result = [_weapons, _magazines, _items, _backpacks];

_result
