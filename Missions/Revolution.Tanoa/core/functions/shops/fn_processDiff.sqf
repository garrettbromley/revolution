/*
    File: fn_processDiff.sqf
    Author: Garrett
    Description:
        Process the differences in the arsenal until they exit the arsenal
*/

private ["_getConfig", "_getDisplayName", "_generateMessage"];

_getConfig = {
    params["_item"];

    switch true do
    {
        case(isClass(configFile >> "CfgMagazines" >> _item)): {"CfgMagazines"};
        case(isClass(configFile >> "CfgWeapons" >> _item)): {"CfgWeapons"};
        case(isClass(configFile >> "CfgVehicles" >> _item)): {"CfgVehicles"};
        case(isClass(configFile >> "CfgGlasses" >> _item)): {"CfgGlasses"};
    };
};

_getDisplayName = {
    private ["_config"];
    params ["_classname"];

    _config = [_classname] call _getConfig;
    getText(configFile >> _config >> _classname >> "displayname");
};

_generateMessage = {
    private ["_message", "_endLine", "_compiledList", "_grandTotal", "_return"];
    params ["_itemList", "_priceList"];
    _message = ""; _endLine = "\n";
    _grandTotal = 0;

    // Go through the item list, make a list of items, count, price
    _compiledList = [];
    {
        private ["_classname", "_quantity", "_cost"];
        _classname = _x;
        _quantity = {_x == _classname} count _itemList;
        _cost = 0;
        {
            if (_x select 0 == _classname) then { _cost = _x select 1; };
        } forEach _priceList;

        _compiledList pushBackUnique [_classname, _quantity, _cost];
    } forEach _itemList;

    // Generate a message now
    {
        private ["_toAdd", "_total", "_displayName"];
        _total = (_x select 1) * (_x select 2);
        _grandTotal = _grandTotal + _total;
        _displayName = [_x select 0] call _getDisplayName;
        _toAdd = format ["$%3 :: x%2 :: %1", _displayName, _x select 1, _total];
        _message = _message + _toAdd + _endLine;
    } forEach _compiledList;

    _message = _message + format ["Total: $%1", _grandTotal];

    _return = [_message, _grandTotal];

    _return;
};

private ["_currentLoadout", "_newLoadout", "_thePrices", "_message", "_cartTotal"];
params ["_shopGunID", "_shopClothingID", "_shopName"];

_message = "";

// Get their loadout going in
_currentLoadout = getUnitLoadout player;

// Get all the items and the prices that we will be seeing
_thePrices = [_shopGunID, _shopClothingID] call shop_fnc_getPrices;

// Wait for them to open the arsenal
waitUntil {!isNull (uiNamespace getVariable "RSCDisplayArsenal")};

// Run the loop
while {!isNull (uiNamespace getVariable "RSCDisplayArsenal")} do {
    private ["_arraysSame", "_newLoadout", "_newItemsPre", "_newItems", "_messageReturn"];

    // Get their new loadout
    _newLoadout = getUnitLoadout player;

    // See if the arrays are the same
    _arraysSame = [_currentLoadout, _newLoadout] call process_fnc_compareArrays;

    // If the arrays are not the same, process the changes
    if !(_arraysSame) then {
        _newItemsPre = [_currentLoadout, _newLoadout] call process_fnc_gearDifferences;

        // Trim any blank strings in the array
        _newItems = [];
        {
            if (_x != "") then { _newItems pushBack _x; };
        } forEach _newItemsPre;

        // Display the differences to them
        _messageReturn = [_newItems, _thePrices] call _generateMessage;
        _message = _messageReturn select 0;
        _cartTotal = _messageReturn select 1;
        titleText [_message, "PLAIN DOWN"];
    };

    sleep 0.5;
};

if (_message != "") then {
    // They exited the arsenal
    [_message, _cartTotal, _currentLoadout, _shopName] spawn shop_fnc_confirmPurchase;
};
