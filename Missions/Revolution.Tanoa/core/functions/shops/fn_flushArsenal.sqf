/*
    File: fn_flushArsenal.sqf
    Author: Garrett
    Description:
        Flushes the cargo contents of the player
        Call before you open any shops
*/

{
    [player,[_x],true] call BIS_fnc_removeVirtualItemCargo;
} forEach (player call BIS_fnc_getVirtualItemCargo);

{
    [player,[_x],true] call BIS_fnc_removeVirtualWeaponCargo;
} forEach (player call BIS_fnc_getVirtualWeaponCargo);

{
    [player,[_x],true] call BIS_fnc_removeVirtualBackpackCargo;
} forEach (player call BIS_fnc_getVirtualBackpackCargo);

{
    [player,[_x],true] call BIS_fnc_removeVirtualMagazineCargo;
} forEach (player call BIS_fnc_getVirtualMagazineCargo);
