/*
    File: fn_civClothingShop.sqf
    Author: Garrett
    Description:
        Return the arsenal information to the caller
*/

private ["_result", "_items", "_backpacks", "_weapons", "_magazines", "_uniforms", "_vests", "_headgear", "_facewear", "_nvg", "_equipment", "_cfgData"];

// Compile list of weapons
_weapons = [];

// Compile list of magazines
_magazines = [];

// Compile list of items
_uniforms = (MissionConfigFile >> "MissionCFGMain" >> "CivClothingShopConfig" >> "uniforms") call BIS_fnc_getCfgData;
_vests = (MissionConfigFile >> "MissionCFGMain" >> "CivClothingShopConfig" >> "vests") call BIS_fnc_getCfgData;
_headgear = (MissionConfigFile >> "MissionCFGMain" >> "CivClothingShopConfig" >> "headgear") call BIS_fnc_getCfgData;
_facewear = (MissionConfigFile >> "MissionCFGMain" >> "CivClothingShopConfig" >> "facewear") call BIS_fnc_getCfgData;
_nvg = (MissionConfigFile >> "MissionCFGMain" >> "CivClothingShopConfig" >> "nvg") call BIS_fnc_getCfgData;
_equipment = (MissionConfigFile >> "MissionCFGMain" >> "CivClothingShopConfig" >> "equipment") call BIS_fnc_getCfgData;

_cfgData = _uniforms + _vests + _headgear + _facewear + _nvg + _equipment;

_items = [];
{
    _items pushBack (_x select 0);
} forEach _cfgData;

// Compile list of backpacks
_cfgData = (MissionConfigFile >> "MissionCFGMain" >> "CivClothingShopConfig" >> "backpacks") call BIS_fnc_getCfgData;

_backpacks = [];
{
    _backpacks pushBack (_x select 0);
} forEach _cfgData;




_result = [_weapons, _magazines, _items, _backpacks];

_result;
