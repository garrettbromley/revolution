/*
    File: fn_openArsenalShop.sqf
    Author: Garrett
    Description:
        Opens the shop in the arsenal
*/

private ["_shopID", "_result", "_items", "_backpacks", "_weapons", "_magazines"];
_shopID = _this select 3;

// Gather the items from the appropriate functions
switch (_shopID) do {
    case "civClothing": { _result = [] call shop_fnc_civClothingShop;  };
    case "civGun": {      _result = [] call shop_fnc_civGunShop;       };
    case "cop": {         _result = [] call shop_fnc_copShop;          };
};

_weapons = _result select 0;
_magazines = _result select 1;
_items = _result select 2;
_backpacks = _result select 3;

// Launch up the arsenal
[] call shop_fnc_flushArsenal;
["Preload"] call BIS_fnc_arsenal;
[player, _items, false, false] call BIS_fnc_addVirtualItemCargo;
[player, _backpacks, false, false] call BIS_fnc_addVirtualBackpackCargo;
[player, _weapons, false, false] call BIS_fnc_addVirtualWeaponCargo;
[player, _magazines, false, false] call BIS_fnc_addVirtualMagazineCargo;
["Open",[nil,player]] spawn BIS_fnc_arsenal;

// Process the changes
switch (_shopID) do {
    case "civClothing": { ["CivGunShopConfig", "CivClothingShopConfig", "Clothing Store"] spawn shop_fnc_processDiff;  };
    case "civGun": {      ["CivGunShopConfig", "CivClothingShopConfig", "Gun Shop"] spawn shop_fnc_processDiff;  };
    case "cop": {         ["CopGunShopConfig", "CopClothingShopConfig", "Cop Shop"] spawn shop_fnc_processDiff;  };
};
