/*
	Author: 

	Description:
	Remove item from inventory

	Parameter(s):
		id:			number - item id
		amount: 	number
		source:		number - 0: backpack   1: hands   2: both

	Returns:
		boolean: true if item was removed
*/