/*
	Author: 

	Description:
	Add item to inventory

	Parameter(s):
		id:			number - item id
		amount: 	number
		purchased:	boolean - true if the item is purchased
		target:		number - 0: backpack   1: hands   2: backpack if there is space, else hands

	Returns:
		boolean: true if item was added
*/