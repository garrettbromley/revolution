#include "Keycodes.h"

private ["_handled", "_ctrl", "_dikCode", "_shift", "_ctrlKey", "_alt"];

_ctrl = _this select 0;
_dikCode = _this select 1;
_shift = _this select 2;
_ctrlKey = _this select 3;
_alt = _this select 4;

_handled = false;
_target = cursorTarget;


//Include here the keys whose default behaviuor is going to be modified.
_overrideKeys = 
[
	DIK_0, DIK_1, DIK_2, DIK_3, DIK_4, DIK_5, DIK_6, DIK_7, DIK_8, DIK_9,
	DIK_F1, DIK_F2, DIK_F3,DIK_F4,DIK_F5,DIK_F6,DIK_F7,DIK_F8,DIK_F9,DIK_F10,DIK_F11,DIK_F12,
	DIK_ESCAPE,
	DIK_GRAVE,
	DIK_J, DIK_M, DIK_P
];

if(_dikCode in _overrideKeys) then {
	_handled = true;
};

_this spawn PL_fnc_asyncKeydown;

_handled