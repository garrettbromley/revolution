#include "Keycodes.h"
disableSerialization;

_unit = [_this, 0, objNull, [objNull]] call BIS_fnc_param;
_killer = [_this, 1, objNull, [objNull]] call BIS_fnc_param;

_this spawn {

	_this call Cinematic_fnc_createDeadCamera;
	[] spawn Music_fnc_playRandom;
	_deadCode = { if((_this select 1) == 1) then [{true}, {false}];};

	[] spawn {
		waitUntil {missionNamespace getVariable["deadCamDone", false]};
		[] call Cinematic_fnc_deleteDeadCamera;
		[] spawn Cinematic_fnc_loginCinematic;
		createDialog "SpawnSelect";
	};
};