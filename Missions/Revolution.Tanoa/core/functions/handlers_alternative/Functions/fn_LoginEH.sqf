#include "Keycodes.h"
disableSerialization;

_disp = (findDisplay 15100);

_diker = _this select 1;
_ret = false;

switch (_diker) do {
	case DIK_ESCAPE: {
		hideLogin = true;
		showBlur = !hideLogin;
		if(hideLogin) then {
			{
				((_disp displayCtrl _x) ctrlShow false);
			} forEach (loginDisp);
		};
		_ret = true;
	};
	
	case DIK_SPACE : {
		hideLogin = false;
		showBlur = !hideLogin;
		if(!hideLogin) then {
			{
				((_disp displayCtrl _x) ctrlShow true);
			} forEach (loginDisp);
		};

		_ret = true;
	};
};

true
