if(isServer) then {
	//Not including this time, I'm sorry
	[] call (compileFinal (preprocessFileLineNumbers "src\EventHandlers\SERVER_EH.inc"));
	[] call PL_fnc_executeEventHandlers;
};

//Control layer
90 cutText ["","PLAIN",90000];

switch (side player) do {
	case west: {
		[] call (compileFinal (preprocessFileLineNumbers "src\EventHandlers\WEST_EH.inc"));
		[] call PL_fnc_executeEventHandlers;
	};
	
	case east: {
		[] call (compileFinal (preprocessFileLineNumbers "src\EventHandlers\EAST_EH.inc"));
		[] call PL_fnc_executeEventHandlers;
	};
	
	case independent: {
		[] call (compileFinal (preprocessFileLineNumbers "src\EventHandlers\INDEP_EH.inc"));
		[] call PL_fnc_executeEventHandlers;
	};
	
	case civilian: {
		[] call (compileFinal (preprocessFileLineNumbers "src\EventHandlers\CIV_EH.inc"));
		[] call PL_fnc_executeEventHandlers;
	};
	
	case sideLogic: {
		[] call (compileFinal (preprocessFileLineNumbers "src\EventHandlers\CIV_EH.inc"));
		[] call PL_fnc_executeEventHandlers;
	};
	
	default {
		for "_i" from 0 to 200 do {
			diag_log "WHAT THE HOLLY FUCK ARMA????";
		};
	};
};