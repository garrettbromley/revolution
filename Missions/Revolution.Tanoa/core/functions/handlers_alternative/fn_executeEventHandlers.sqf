{
	_x spawn {
		if(!isNil "_this") then {
			player addEventHandler _this;
		};
	};
} forEach (playerEH);

{
	_x spawn {
		if(!isNil "_this") then {
			disableSerialization;
			_display = (_this deleteAt 0);
			(findDisplay _display) displayAddEventHandler _this;
		};
	};
} forEach (displayEH);

{
	_x spawn {
		if(!isNil "_this") then {
			addMissionEventHandler _this;
		};
	};
} forEach (missionEH);