/*
    File: fn_licenses.sqf
    Author: Garrett
    Description:
        Tells the server to update the player's licenses in the database
*/

[getPlayerUID player, civLicenses, copLicenses, medLicenses] remoteExec ["Query_fnc_updatePlayerLicenses", 2];
