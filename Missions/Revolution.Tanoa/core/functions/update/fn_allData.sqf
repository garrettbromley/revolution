/*
    File: fn_allData.sqf
    Author: Garrett
    Description:
        Tells the server to update all the player's data in the database
*/
// Save the things that will be different at this moment
[activeGear] call gear_fnc_saveGear;
cPosition = getPos player;

[
    getPlayerUID player,
    cash,
    bank,
    creditBalance,
    creditLimit,
    civLicenses,
    copLicenses,
    medLicenses,
    civGear,
    copGear,
    medGear,
    activeGear,
    activeRole,
    stats,
    arrested,
    isAlive,
    cPosition,
    playtime
] remoteExec ["Query_fnc_updateAllPlayerData", 2];
