/*
    File: fn_gear.sqf
    Author: Garrett
    Description:
        Tells the server to update the player's gear in the database
*/

[activeGear] call gear_fnc_saveGear;

[getPlayerUID player, civGear, copGear, medGear, activeGear] remoteExec ["Query_fnc_updatePlayerGear", 2];
