/*
    File: fn_money.sqf
    Author: Garrett
    Description:
        Tells the server to update the player's money in the database
*/

[getPlayerUID player, cash, bank, creditBalance, creditLimit] remoteExec ["Query_fnc_updatePlayerMoney", 2];
