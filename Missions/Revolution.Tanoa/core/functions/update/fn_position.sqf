/*
    File: fn_position.sqf
    Author: Garrett
    Description:
        Tells the server to update the player's position in the database
*/

[getPlayerUID player, getPos player] remoteExec ["Query_fnc_updatePlayerPosition", 2];
