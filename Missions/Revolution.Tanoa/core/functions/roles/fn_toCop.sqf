/*
    File: fn_toCop.sqf
    Author: Garrett
    Description:
        Changes to the cop role
*/

// Already on duty!
if (activeRole == "cop") exitWith {};

// Fines due
if (fines > 0) exitWith { hint "You have unpaid fines that are due. Please pay those before going On-Duty"; };

// Remove radio channels
// Add police radio channels
[activeRole, "cop"] call radio_fnc_change;

[true] call cop_fnc_policeMarkers;

// Change status to on-duty
activeRole = "cop";
player setVariable ["isCop", true, true];

hint "You are now an On-Duty Police Officer.";

// They are still in civ gear, tell them to get their police gear
if (activeGear == "civ") then {
    hint "Please report to the equipment officer for your police gear.";
};

// Save status in the database
[getPlayerUID player, activeRole] remoteExec ["Query_fnc_updatePlayerRole", 2];
