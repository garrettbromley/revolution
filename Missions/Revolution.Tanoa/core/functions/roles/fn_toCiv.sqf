/*
    File: fn_toCiv.sqf
    Author: Garrett
    Description:
        Changes to the civ role
*/

private _prevRole = activeRole;

// Already off duty!
if (activeRole == "civ") exitWith {};

// Remove radio channels
// Add civilian radio channels
[activeRole, "civ"] call radio_fnc_change;

// Change status to off-duty
activeRole = "civ";
player setVariable ["isCop", false, true];

hint "You are now Off-Duty.";

// They are still in cop gear, tell them to get their civ clothing
if (activeGear == "cop" || activeGear == "med") then {
    hint "Please report to the equipment officer for your civilian clothing.";
};

// Save status in the database
[getPlayerUID player, activeRole] remoteExec ["Query_fnc_updatePlayerRole", 2];

// If they come from being a cop, start the speed cameras and delete markers
if (_prevRole == "cop") then {
    [] spawn misc_fnc_loadSpeedCameras;
    [false] call cop_fnc_policeMarkers;
};
