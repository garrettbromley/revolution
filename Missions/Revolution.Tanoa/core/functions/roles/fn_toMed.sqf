/*
    File: fn_toMed.sqf
    Author: Garrett
    Description:
        Changes to the medic role
*/

private _prevRole = activeRole;

// Already on duty!
if (activeRole == "med") exitWith {};

// Remove radio channels
// Add medic radio channels
[activeRole, "med"] call radio_fnc_change;

// Change status to on-duty
activeRole = "med";
player setVariable ["isCop", false, true];

hint "You are now an On-Duty Medic.";

// They are still in civ gear, tell them to get their medic gear
if (activeGear == "civ") then {
    hint "Please report to the equipment officer for your medic gear.";
};

// Save status in the database
[getPlayerUID player, activeRole] remoteExec ["Query_fnc_updatePlayerRole", 2];

// If they come from being a cop, start the speed cameras and delete markers
if (_prevRole == "cop") then {
    [] spawn misc_fnc_loadSpeedCameras;
    [false] call cop_fnc_policeMarkers;
};
