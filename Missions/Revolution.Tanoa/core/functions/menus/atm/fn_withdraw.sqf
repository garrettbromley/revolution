/*
	Author: Jannis (Tschuuut)

	Description:
	Withdraw money from bank account

	Parameter(s):
		amount:						number - how much money will be withdrawn

	Returns:
		boolean - true withdraw succeeded
*/

private ["_amount"];

_amount = param[0,0,[0]];

if (_amount < 1) exitWith {false;};

if(bank < _amount) then {
	cash = cash + bank;
	bank = 0;
} else {
	bank = bank - _amount;
	cash = cash + _amount;
};

[] call update_fnc_money;
[getPlayerUID player, getPlayerUID player, _amount, "Withdrawal"] remoteExec ["Query_fnc_insertTransaction", 2];

true;