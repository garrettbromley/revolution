/*
	Author: Jannis (Tschuuut)

	Description:
	Transfer money to another bank account

	Parameter(s):
		amount:				number - how much money will be withdrawn
		target:				object - the person wich will get the money

	Returns:
		boolean - true withdraw succeeded
*/

private ["_amount"];

_amount = param[0,0,[0]];
_target = param[1,objNull,[objNull]];

if(_amount < 1) exitWith {false;};
if(!isPlayer _target) exitWith {false;};
if(bank < _amount) exitWith {false;};

bank = bank - _amount;

[_target, _amount] call bank_fnc_sendMoney;

[] call update_fnc_money;
true;