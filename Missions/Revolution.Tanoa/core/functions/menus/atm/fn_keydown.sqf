/*
	Author: Jannis (Tschuuut)

	Description:
	Updates atm text field

	Parameter(s):
		display:	display - atm display
		key:		number - pressed keycode
		shift:		boolean - shift key pressed
		control:	boolean - control key pressed
		alt:		boolean - alt key pressed

	Returns:
		nothing
*/
disableSerialization;

private ["_display","_edit","_pressed"];

_display = _this select 0;
_edit = _display displayCtrl 10003;

_num = switch (_this select 1) do {
	case 11: {0};
	case 82: {0};
	case  2: {1};
	case 79: {1};
	case  3: {2};
	case 80: {2};
	case  4: {3};
	case 81: {3};
	case  5: {4};
	case 75: {4};
	case  6: {5};
	case 76: {5};
	case  7: {6};
	case 77: {6};
	case  8: {7};
	case 71: {7};
	case  9: {8};
	case 72: {8};
	case 10: {9};
	case 73: {9};
	case 14: {-1};	//Backspace
	default {nil};
};

if(isNil "_num") exitWith {};

if(_num == -1) then {
	atmAmount = floor(atmAmount / 10);
} else {
	atmAmount = atmAmount * 10 + _num;
};

atmAmount = [atmAmount,0,999999] call Misc_fnc_clamp;

_edit ctrlSetText format["$ %1", [atmAmount] call Misc_fnc_formatNumber];