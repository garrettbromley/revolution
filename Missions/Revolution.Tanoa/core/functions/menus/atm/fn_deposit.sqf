/*
	Author: Jannis (Tschuuut)

	Description:
	Deposit money on bank account

	Parameter(s):
		amount:					number - how much money will be deposited

	Returns:
		boolean - true if deposit succeeded
*/

private ["_amount"];

_amount = param[0,0,[0]];

if (_amount < 1) exitWith {false;};

if(cash < _amount) then {
	bank = bank + cash;
	cash = 0;
} else {
	cash = cash - _amount;
	bank = bank + _amount;
};

[] call update_fnc_money;
[getPlayerUID player, getPlayerUID player, _amount, "Deposit"] remoteExec ["Query_fnc_insertTransaction", 2];

true;