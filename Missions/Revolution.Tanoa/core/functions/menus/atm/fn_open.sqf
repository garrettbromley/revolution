/*
	Author: Jannis (Tschuuut)

	Description:
	Open ATM menu

	Parameter(s):
		none

	Returns:
		nothing
*/
disableSerialization;
if(!((findDisplay 10000) isEqualTo displayNull)) exitWith {};		//Allready open
createDialog "atm_menu";
waitUntil{!((findDisplay 10000) isEqualTo displayNull)};
private["_display","_welcome","_account","_user"];

_display = findDisplay 10000;
_welcome = _display displayCtrl 10001;
_account = _display displayCtrl 10002;
_user = _display displayCtrl 10004;

_welcome ctrlSetText format["Welcome to SUATMM, %1! Never give your account credentials to anyone!",name player];
_account ctrlSetText format["$ %1",[bank] call Misc_fnc_formatNumber];

atmPlayers = allPlayers;
{
	if(_x != player) then {
		_user lbAdd name _x;
	};
} forEach atmPlayers;