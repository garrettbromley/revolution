/*
	Author: Jannis (Tschuuut)

	Description:
	Update inputfield on keydown event in dialog
	
	Important:
	You need to unset the keydownAmount variable on unload

	Parameter(s):
		keydownParameter:	Array - _this in keydown event
				display:		display - current display
				key:			number - pressed keycode
				shift:			boolean - shift key pressed
				control:		boolean - control key pressed
				alt:			boolean - alt key pressed
		editText:			Number - idc of the output textfield
		type:				String - type of the output text
								"money" -> $ XX.XXX.XXX
								"amount" -> XX.XXX.XXX
								"none" -> XXXXX (Default)
		maxValue: (Optional)Number - highest possible value (Default: 999.999)
	Returns:
		nothing
*/
disableSerialization;

private ["_display","_edit","_pressed","_type"];

_display = (_this select 0) select 0;
_edit = _display displayCtrl (param[1,1,[1]]);
_type = param[2,"",[""]];
_maxValue = param[3,999999,[0]];

_num = switch ((_this select 0) select 1) do {
	case 11: {0};
	case 82: {0};
	case  2: {1};
	case 79: {1};
	case  3: {2};
	case 80: {2};
	case  4: {3};
	case 81: {3};
	case  5: {4};
	case 75: {4};
	case  6: {5};
	case 76: {5};
	case  7: {6};
	case 77: {6};
	case  8: {7};
	case 71: {7};
	case  9: {8};
	case 72: {8};
	case 10: {9};
	case 73: {9};
	case 14: {-1};	//Backspace
	default {nil};
};

if(isNil "_num") exitWith {};
if(isNil "keydownAmount") then {keydownAmount = 0;};

if(_num == -1) then {
	keydownAmount = floor(keydownAmount / 10);
} else {
	keydownAmount = keydownAmount * 10 + _num;
};

keydownAmount = [keydownAmount,0,_maxValue] call Misc_fnc_clamp;

_edit ctrlSetText (switch (_type) do {
	case "money": {
		format["$ %1", [keydownAmount] call Misc_fnc_formatNumber];
	};
	case "amount": {
		format["%1", [keydownAmount] call Misc_fnc_formatNumber];
	};
	default {
		str keydownAmount;
	};
});