/*
	Author: Jannis (Tschuuut)

	Description:
	Update inventory slots

	Parameter(s):
		nothing

	Returns:
		nothing
*/

if(!((findDisplay 11000) isEqualTo displayNull)) exitWith {}; //Display allready exists
createDialog "inventory";
disableSerialization;
_dialog = findDisplay 11000;

_licenses = _dialog displayCtrl 11002;

_giveType = _dialog displayCtrl 11200;
_givePlayer = _dialog displayCtrl 11201;

_giveType lbAdd "Money";
_giveType lbAdd "Item";
_giveType lbSetCurSel 0;

//Add licenses
{
	_licenses lbAdd (getText("missionConfigFile" >> "Licenses" >> _x >> "displayName"));
} forEach (switch (activeRole) do {case "civ": {civLicenses;}; case "cop": {copLicenses;}; case "med": {medLicenses;};});

_cursor = _dialog displayCtrl 11100;

_backpackSlots = [];
for "_i" from 11110 to 11113 do {
	_backpackSlots pushBack (_dialog displayCtrl _i);
};

_handSlots = [];
for "_i" from 11120 to 11127 do {
	_handSlots pushBack (_dialog displayCtrl _i);
};

_cursor ctrlSetPosition (ctrlPosition (_backpackSlots select 0));
_cursor ctrlCommit 0;

selectedItem = ctrlIDC(_backpackSlots select 0);

//Init playerlist
invPlayers = allPlayers;
{
	if(_x != player) then {
		if((player distanceSqr _x) < 9) then {
			if(lineIntersects [eyePos player, eyePos _x, player, _x]) then {
				_givePlayer lbAdd name _x;
			};
		};
	};
} forEach invPlayers;

[] call inv_fnc_update;