/*
	Author: Jannis (Tschuuut)

	Description:
	Set Cursor

	Parameter(s):
		default _this variable in onMouseButtonDown event

	Returns:
		nothing
*/

private["_dialog","_cursor","_target"];
if((findDisplay 11000) isEqualTo displayNull) exitWith {}; //Display dont exists
_dialog = findDisplay 11000;
_cursor = _dialog displayCtrl 11100;
_target = false;

_cursorPos = [_this select 2, _this select 3];

for "_i" from 11110 to 11113 do {
	_ctrl = (_dialog displayCtrl _i);
	_pos = ctrlPosition _ctrl;
	if(_cursorPos select 0 > _pos select 0 && _cursorPos select 0 < (_pos select 0) + (_pos select 2) && _cursorPos select 1 > _pos select 1 && _cursorPos select 1 < (_pos select 1) + (_pos select 3)) exitWith {
		_target = _ctrl;
	};
};

if(_target isEqualTo false) then {
	for "_i" from 11120 to 11127 do {
		_ctrl = (_dialog displayCtrl _i);
		_pos = ctrlPosition _ctrl;
		if(_cursorPos select 0 > _pos select 0 && _cursorPos select 0 < (_pos select 0) + (_pos select 2) && _cursorPos select 1 > _pos select 1 && _cursorPos select 1 < (_pos select 1) + (_pos select 3)) exitWith {
			_target = _ctrl;
		};
	};
};

if(!(_target isEqualTo false)) then {
	_cursor ctrlSetPosition (ctrlPosition _target);
	_cursor ctrlCommit 0.1;
	selectedItem = ctrlIDC _target;
};