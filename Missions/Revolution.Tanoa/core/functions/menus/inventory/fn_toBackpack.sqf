/*
	Author: Jannis (Tschuuut)

	Description:
	Move item from hands to backpack
	
	Important:
	This function is meant to be called from the inventory dialog

	Parameter(s):
		nothing

	Returns:
		nothing
*/
if(selectedItem < 11120) exitWith {};	//Item in hands is selected
_index = (selectedItem - 11120);
if(_index > count playerHands) exitWith {};		//Empty slot selected
_item = playerHands select _index;

if(count playerInv < 4) then {
	playerInv pushBack _item;
	playerHands deleteAt _index;
};

[] call inv_fnc_update;