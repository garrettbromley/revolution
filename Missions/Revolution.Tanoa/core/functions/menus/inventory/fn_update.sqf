/*
	Author: Jannis (Tschuuut)

	Description:
	Update inventory slots

	Parameter(s):
		nothing

	Returns:
		nothing
*/

private["_dialog","_backpackSlots","_handSlots","_backpackSlotsAmount","_backpackSlotsAmount"];
if((findDisplay 11000) isEqualTo displayNull) exitWith {}; //Display dont exists
_dialog = findDisplay 11000;

_cash = _dialog displayCtrl 11003;
_bank = _dialog displayCtrl 11004;
_health = _dialog displayCtrl 11005;
_hunger = _dialog displayCtrl 11006;

_cash ctrlSetText ([cash] call misc_fnc_formatNumber);
_bank ctrlSetText ([bank] call misc_fnc_formatNumber);
_health ctrlSetText format["%1%2",100 - (floor ((damage player) * 100)),"%"];
_hunger ctrlSetText format["%1%2",0,"%"];

//Define backpack slots
_backpackSlots = [];
for "_i" from 11110 to 11113 do {
	_backpackSlots pushBack (_dialog displayCtrl _i);
};

//Define backpack slot amounts
_backpackSlotsAmount = [];
for "_i" from 11130 to 11133 do {
	_backpackSlotsAmount pushBack (_dialog displayCtrl _i);
};

//Define hand slots
_handSlots = [];
for "_i" from 11120 to 11127 do {
	_handSlots pushBack (_dialog displayCtrl _i);
};

//Define hand slot amounts
_handSlotsAmount = [];
for "_i" from 11140 to 11147 do {
	_handSlotsAmount pushBack (_dialog displayCtrl _i);
};

//Set item image and item amount in menu for backpack
{
	(_backpackSlots select _forEachIndex) ctrlSetText "media\icons\placeholder.paa";
	(_backpackSlotsAmount select _forEachIndex) ctrlSetText format["%2x %1",_x select 0, _x select 1];
} forEach playerInv;
//Reset remaining backpack slots to default
for "_i" from (count playerInv) to 3 do {
	(_backpackSlots select _i) ctrlSetText "media\icons\empty.paa";
	(_backpackSlotsAmount select _i) ctrlSetText "";
};

{
	(_handSlots select _forEachIndex) ctrlSetText "media\icons\placeholder.paa";
	(_handSlotsAmount select _forEachIndex) ctrlSetText format["%2x %1",_x select 0, _x select 1];
} forEach playerHands;
//Reset remaining hand slots to default
for "_i" from (count playerHands) to 7 do {
	(_handSlots select _i) ctrlSetText "media\icons\empty.paa";
	(_handSlotsAmount select _i) ctrlSetText "";
};