/*
	Author: Jannis (Tschuuut)

	Description:
	Move item from backpack to hands
	
	Important:
	This function is meant to be called from the inventory dialog

	Parameter(s):
		nothing

	Returns:
		nothing
*/

if(selectedItem > 11119) exitWith {};	//Item in backpack is selected
_index = (selectedItem - 11110);
if(_index > count playerInv) exitWith {};		//Empty slot selected
_item = playerInv select _index;

if(count playerHands < 8) then {
	playerHands pushBack _item;
	playerInv deleteAt _index;
};

[] call inv_fnc_update;