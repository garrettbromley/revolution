scriptName "fn_getVehicleIndicatorOffsets";
#define __filename "fn_getVehicleIndicatorOffsets.sqf"

_class = param[0,"",[""]];

// Ex
if (_class == "") exitWith {[[0,0,0],[0,0,0],[0,0,0],[0,0,0]]};

// Get config entry
_config = getArray(missionConfigFile >> "MissionCFGMain" >> "IndicatorPositions" >> "Vehicles" >> _class >> "positions");

// Ex
if (count _config == 0) exitWith {[[0,0,0],[0,0,0],[0,0,0],[0,0,0]]};

// Return
_config;
