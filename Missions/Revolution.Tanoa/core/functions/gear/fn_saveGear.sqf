/*
    File: fn_saveGear.sqf
    Author: Garrett
    Description:
        Saves the gear to the specified class
*/

params ["_gearType"];

switch (_gearType) do {
    case "civ": { civGear = format ["%1", getUnitLoadout player]; };
    case "cop": { copGear = format ["%1", getUnitLoadout player]; };
    case "med": { medGear = format ["%1", getUnitLoadout player]; };
};
