/*
    File: fn_assignGear.sqf
    Author: Garrett
    Description:
        Assigns the appropriate gear to the player
*/

params ["_gearType"];

private ["_gearToSet", "_gearFinal"];
switch (_gearType) do {
    case "civ": { _gearToSet = civGear; };
    case "cop": { _gearToSet = copGear; };
    case "med": { _gearToSet = medGear; };
    default {     _gearToSet = civGear; };
};

if (typeName _gearToSet == "STRING") then {
	_gearFinal = call compile _gearToSet;
};

if (typeName _gearToSet == "ARRAY") then {
	_gearFinal = _gearToSet;
};

player setUnitLoadout _gearFinal;
