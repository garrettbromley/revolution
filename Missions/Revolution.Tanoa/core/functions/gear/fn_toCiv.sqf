/*
    File: fn_toCiv.sqf
    Author: Garrett
    Description:
        Changes the gear to a civ
*/

// Save current gear
[activeGear] call gear_fnc_saveGear;

// Change Active Gear Mode
activeGear = "civ";

player playMove "AmovPercMstpSnonWnonDnon_AinvPercMstpSnonWnonDnon_Putdown";
sleep 0.6;
player playMove "AinvPknlMstpSnonWnonDnon_medic_1";

// Assign the active gear to the user
[activeGear] call gear_fnc_assignGear;

// They are still a cop/medic role, tell them to go off-duty
if (activeRole == "cop" || activeRole == "med") then {
    hint "You are still On-Duty. Report to the front desk to change your active status!";
};

// Save active gear in the database
[] call update_fnc_gear;
