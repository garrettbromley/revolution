/*
    File: fn_addFine.sqf
    Author: Garrett
    Description:
        Tells the server to insert the player's fine in the database
*/

params ["_amount"];

[getPlayerUID player, name player, _amount] remoteExec ["Query_fnc_insertFine", 2];
