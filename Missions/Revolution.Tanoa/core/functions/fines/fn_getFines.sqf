/*
    File: fn_getFines.sqf
    Author: Garrett
    Description:
        Processes the server's query fine total
*/

params ["_total"];

if (typeName ((_total select 0) select 0) == "SCALAR") then {
    fines = ((_total select 0) select 0);
} else {
    fines = 0;
};
