/*
    File: fn_payFines.sqf
    Author: Garrett
    Description:
        Asks if they wish to pay their fines.
        Tells the server to pay all the player's fines in the database
*/

private ["_message", "_decision"];

// Ask if they want to pay fines
_message = format ["Do you wish to pay your fines of $%1<br />Note: Any unpaid fines puts you at risk of becoming wanted by the police over time.", fines];


_decision = [_message, "PAY FINES", "YES", "NO"] call BIS_fnc_guiMessage;

// Process their decision
if (_decision) then {
    // Take their money
    if ((cash - fines) < 0) then {
        // Revert them to their previous loadout
        hint format ["Not enough money for your fines!\nMissing $%1", ((cash - _total) * -1)];
    } else {
        // Take money
        cash = cash - fines;

        // Save their money and gear in the database
        [] call update_fnc_money;
        [getPlayerUID player] remoteExec ["Query_fnc_updateFines", 2];
        fines = 0;
    };
} else {
    hint "You still have unpaid fines, deliquent fines may cause you to become wanted by the police.";
};
