/*
    File: fn_requestFines.sqf
    Author: Garrett
    Description:
        Sends request to server for player fines
*/

private ["_uid", "_sender"];

// Gather player information to send to server so it knows where to go back to
_sender = player;
_uid = getPlayerUID _sender;

// Tell the server to check for player in the database
[_uid, _sender] remoteExec ["Query_fnc_getFines", 2];
