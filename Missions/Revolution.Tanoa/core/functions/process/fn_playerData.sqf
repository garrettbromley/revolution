/*
    File: fn_playerData.sqf
    Author: Garrett
    Description:
        Processes the server's query for the player's data
        Sets up the client
*/

params ["_playerData"];

_playerData = _playerData select 0;

// Set the variables to the returned data
cash        = _playerData select 0;
bank        = _playerData select 1;
creditBalance= _playerData select 2;
creditLimit = _playerData select 3;
[] call fines_fnc_requestFines;
copLevel    = _playerData select 4;
medicLevel  = _playerData select 5;
civLicenses = call compile (_playerData select 6);
copLicenses = call compile (_playerData select 7);
medLicenses = call compile (_playerData select 8);
civGear     = _playerData select 9;
copGear     = _playerData select 10;
medGear     = _playerData select 11;
activeGear  = _playerData select 12;
activeRole  = _playerData select 13;
stats       = call compile (_playerData select 14);
arrested    = _playerData select 15;
adminLevel  = 1;    // For testing
// adminLevel  = _playerData select 16;
donorLevel  = _playerData select 17;
isAlive     = _playerData select 18;
cPosition   = _playerData select 19;
playtime    = _playerData select 20;
startTime   = time;
blacklist   = _playerData select 21;
playerInv = [["apple",1,0],["apple",4,1],["steak",6,4]];		//inventory varialbles used in the inventory system. 
playerHands = [["barrier",1,0]];								//format: ["item name",item count]

// Assign the active gear to the user
[activeGear] call gear_fnc_assignGear;

// Setup Radio Channels
[] call radio_fnc_initialize;

// Set the players position to last recorded
player setPosATL cPosition;

// Welcome to the world
[] spawn misc_fnc_introShot;

// Setup some variables
if (activeRole == "cop") then { 
    player setVariable ["isCop", true, true]; 
    [true] call cop_fnc_policeMarkers;
} else { 
    player setVariable ["isCop", false, false]; 
};

isTazed = false;
isKnockedOut = false;
sirenOn = false;
player setVariable ["Escorting", false, true];
player setVariable ["Restrained", false, true];
