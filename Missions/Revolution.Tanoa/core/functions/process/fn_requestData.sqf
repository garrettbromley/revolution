/*
    File: fn_requestData.sqf
    Author: Garrett
    Description:
        Sends request to server for player information
*/

private ["_uid", "_sender"];

// Gather player information to send to server so it knows where to go back to
_sender = player;
_uid = getPlayerUID _sender;

// Notify the player we are still getting stuff
cutText["Requesting your data from the server...", "BLACK FADED"];
0 cutFadeOut 999999999;

// Tell the server to check for player in the database
[_uid, _sender] remoteExec ["Query_fnc_checkPlayer", 2];
