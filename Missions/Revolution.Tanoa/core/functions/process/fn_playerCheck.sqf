/*
    File: fn_playerCheck.sqf
    Author: Garrett
    Description:
        Processes response from server.
        If client exists, then have them pull data
        If client doesn't exist, have it make a new entry in the database
*/

private ["_sender", "_pid", "_name", "_cash", "_bank","_position"];
params ["_playerExists"];

// Gather player information to send to server so it knows where to go back to
_sender = player;
_pid = getPlayerUID _sender;

if (_playerExists) then {
    // Have them query the data
    [_pid, _sender] remoteExec ["Query_fnc_getPlayerData", 2];
} else {
    [] call Process_fnc_newPlayerSetup;
};
