/*
    File: fn_newPlayerSetup.sqf
    Author: Garrett
    Description:
        Had new entry created in database
        Now we need to setup the client with default settings
*/

private ["_startingUniforms", "_startingPos", "_startingDir"];

// Setup Money
cash = (MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "startingCash") call BIS_fnc_getCfgData;
bank = (MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "startingBank") call BIS_fnc_getCfgData;
creditBalance = 0;
creditLimit = (MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "startingCreditLimit") call BIS_fnc_getCfgData;
fines       = 0;

_startingPos = call compile ((MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "startingPos") call BIS_fnc_getCfgData);
_startingDir = (MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "startingDir") call BIS_fnc_getCfgData;

// Setup Misc Default Variables
copLevel    = 0;
player setVariable ["isCop", false, true];
medicLevel  = 0;
civLicenses = [];
copLicenses = [];
medLicenses = [];
stats       = [100, 100, 0];
arrested    = 0;
adminLevel  = 1;    // For testing
donorLevel  = 0;
isAlive     = 1;
isTazed     = false;
isKnockedOut= false;
sirenOn     = false;
player setVariable ["Escorting", false, true];
player setVariable ["Restrained", false, true];
cPosition   = _startingPos;
playtime    = 0;
startTime = time;
blacklist   = 0;

// Pick a random uniform from what we allow
_startingUniforms = call compile ((MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "startingUniforms") call BIS_fnc_getCfgData);
removeUniform player;
player forceAddUniform (selectRandom _startingUniforms);

// Put the player where the new spawn is
player setPosATL _startingPos;
player setDir _startingDir;

// Save their gear
civGear = format ["%1", getUnitLoadout player];
copGear = call compile ((MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "defaultCopGear") call BIS_fnc_getCfgData);
medGear = call compile ((MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "defaultMedGear") call BIS_fnc_getCfgData);
activeGear = "civ";
activeRole = "civ";

// Have them enter a new player
[getPlayerUID player, name player, cash, bank, creditBalance, creditLimit, cPosition, civGear, copGear, medGear, activeGear, activeRole] remoteExec ["Query_fnc_insertPlayer", 2];

// Setup Radio Channels
[] call radio_fnc_initialize;

// Welcome to the world
[] spawn misc_fnc_introShot;
