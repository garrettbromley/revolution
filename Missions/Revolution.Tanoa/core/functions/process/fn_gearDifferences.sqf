/*
    File: fn_gearDifferences.sqf
    Author: Garrett
    Description:
        Returns an array of the differences in the player's gear
*/

private ["_return", "_old", "_combination", "_additions", "_deletions", "_changedItems"];

params ["_oldGear", "_newGear"];

_return = [];

check_clothing_contents = {
    params ["_ID"];

    // Check for uniform differences
    _old = _oldGear select _ID;
    _new = _newGear select _ID;
    // If there is a uniform to look at
    if !(_new isEqualTo []) then {
        // If the uniform has changed
        if !(_old isEqualTo []) then {
            _old = _oldGear select _ID select 0; _new = _newGear select _ID select 0;
            if (_old != _new) then { _return pushBack _new; };

            // Check for uniform content differences
            _old = _oldGear select _ID select 1; _new = _newGear select _ID select 1;
            _combination = _old arrayIntersect _new;
            _additions = _new - _combination;
            _deletions = _old - _combination;

            // Decipher changes versus additions
            _changedItems = [];
            {
                private ["_changed", "_oldQuantity", "_addedItem", "_addedItemQuantity"];
                _changed = false;
                _oldQuantity = 0;
                _addedItem = _x select 0;
                _addedItemQuantity = _x select 1;
                {
                    if (_addedItem == _x select 0) then { _changed = true; _oldQuantity = _x select 1; };
                } forEach _deletions;

                // If it is a changed item, then get the difference in quantity and push it to the changed items array
                if (_changed) then {
                    _changedItems pushBack [_addedItem, _addedItemQuantity - _oldQuantity];
                } else {
                    // It is a new item, add the quantity and push it to the new items array
                    _changedItems pushBack [_addedItem, _addedItemQuantity];
                };
            } forEach _additions;

            // Add them to the return
            {
                private ["_classname", "_quantity"];
                _classname = _x select 0;
                _quantity = _x select 1;
                for [{_i = 0}, {_i < _quantity}, {_i = _i + 1}] do
                {
                    _return pushBack _classname;
                };
            } forEach _changedItems;
        } else {
            // Everything is new, so put everything in
            // Uniform classname
            _new = _newGear select _ID select 0;
            _return pushBack _new;

            // Uniform Contents
            _new = _newGear select _ID select 1;
            // Get the quantity of all the contents
            _newItems = [];
            {
                private ["_addedItem", "_addedItemQuantity"];
                _addedItem = _x select 0;
                _addedItemQuantity = _x select 1;
                // It is a new item, add the quantity and push it to the new items array
                _newItems pushBack [_addedItem, _addedItemQuantity];
            } forEach _new;

            // Add them to the return
            {
                private ["_classname", "_quantity"];
                _classname = _x select 0;
                _quantity = _x select 1;
                for [{_i = 0}, {_i < _quantity}, {_i = _i + 1}] do
                {
                    _return pushBack _classname;
                };
            } forEach _newItems;
        };
    };
};

// Check primary weapon differences
_old = _oldGear select 0; _new = _newGear select 0;
_combination = _old arrayIntersect _new;
_additions = _new - _combination;
{
    // If it is a string, add it
    if (typeName _x == "STRING") then {
        _return pushBack _x;
    };

    // If it is an array, add the first value (magazines)
    if (typeName _x == "ARRAY") then {
        _return pushBack (_x select 0);
    };

} forEach _additions;

// Check for secondary weapon differences
_old = _oldGear select 1; _new = _newGear select 1;
_combination = _old arrayIntersect _new;
_additions = _new - _combination;
{
    // If it is a string, add it
    if (typeName _x == "STRING") then {
        _return pushBack _x;
    };

    // If it is an array, add the first value (magazines)
    if (typeName _x == "ARRAY") then {
        _return pushBack (_x select 0);
    };

} forEach _additions;

// Check for hand gun weapon differences
_old = _oldGear select 2; _new = _newGear select 2;
_combination = _old arrayIntersect _new;
_additions = _new - _combination;
{
    // If it is a string, add it
    if (typeName _x == "STRING") then {
        _return pushBack _x;
    };

    // If it is an array, add the first value (magazines)
    if (typeName _x == "ARRAY") then {
        _return pushBack (_x select 0);
    };

} forEach _additions;

// Check for uniform differences
[3] call check_clothing_contents;

// Check for vest differences
[4] call check_clothing_contents;

// Check for backpack differences
[5] call check_clothing_contents;

// Check helmet
_old = _oldGear select 6; _new = _newGear select 6;
if (_old != _new) then { _return pushBack _new; };

// Check facewear
_old = _oldGear select 7; _new = _newGear select 7;
if (_old != _new) then { _return pushBack _new; };

// Check binoculars
_old = _oldGear select 8;
_new = _newGear select 8;
if !(_new isEqualTo []) then {
    if !(_old isEqualTo []) then {
        _old = _oldGear select 8 select 0; _new = _newGear select 8 select 0;
        if (_old != _new) then { _return pushBack _new; };
    } else {
        _new = _newGear select 8 select 0;
        _return pushBack _new;
    };
};

// Check Map
_old = _oldGear select 9 select 0; _new = _newGear select 9 select 0;
if (_old != _new) then { _return pushBack _new; };

// Check GPS
_old = _oldGear select 9 select 1; _new = _newGear select 9 select 1;
if (_old != _new) then { _return pushBack _new; };

// Check Radio
_old = _oldGear select 9 select 2; _new = _newGear select 9 select 2;
if (_old != _new) then { _return pushBack _new; };

// Check Compass
_old = _oldGear select 9 select 3; _new = _newGear select 9 select 3;
if (_old != _new) then { _return pushBack _new; };

// Check Watch
_old = _oldGear select 9 select 4; _new = _newGear select 9 select 4;
if (_old != _new) then { _return pushBack _new; };

// Check NVGs
_old = _oldGear select 9 select 5; _new = _newGear select 9 select 5;
if (_old != _new) then { _return pushBack _new; };

// Return all the differences
_return;
