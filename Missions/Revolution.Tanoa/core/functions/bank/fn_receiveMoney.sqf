/*
    File: fn_receiveMoney.sqf
    Author: Garrett
    Description:
        Receive money from another player
*/

params ["_amount"];

private _waitEnabled = (MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "enableWait") call BIS_fnc_getCfgData;

if (_waitEnabled) then {
    [] spawn {
        private _timeToWait = (MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "waitPeriod") call BIS_fnc_getCfgData;
        sleep _timeToWait * 60; // Convert to seconds
        bank = bank + _amount;
        // TODO: Notify them of the wire?
        [] call update_fnc_money;
    };
} else {
    bank = bank + _amount;
    // TODO: Notify them of the wire?
    [] call update_fnc_money;
};
