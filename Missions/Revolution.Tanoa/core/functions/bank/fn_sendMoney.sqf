/*
    File: fn_sendMoney.sqf
    Author: Garrett
    Description:
        Send money to another player
*/

params ["_toID", "_amount"];

[_amount] remoteExecCall ["bank_fnc_receiveMoney", (owner _toID)];
[getPlayerUID player, _toID, _amount, "Sent/Received"] remoteExec ["Query_fnc_insertTransaction", 2];
