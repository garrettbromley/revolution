/*
    File: fn_restrainAction.sqf
    Author: Garrett
    Description:
        Restrains the target
*/

private ["_unit"];
_unit = cursorObject;
if (isNull _unit) exitWith {}; //Not valid
if (player distance _unit > 3) exitWith {};
if (_unit getVariable ["restrained", false]) exitWith {};
if (_unit getVariable ["isCop", false]) exitWith {};
if (player isEqualTo _unit) exitWith {};
if (!isPlayer _unit) exitWith {};
//Broadcast!

_unit setVariable ["Restrained", true, true];
[player] remoteExec ["civ_fnc_restrained", _unit];
