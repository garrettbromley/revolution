/*
    File: fn_siren.sqf
    Author: Garrett
    Description:
        Starts the cop siren sound for other players.
*/

private ["_vehicle", "_firstRun"];
_vehicle = param [0,objNull,[objNull]];

if (isNull _vehicle) exitWith {};
if (isNil {_vehicle getVariable "siren"}) exitWith {};
if (isNil {_vehicle getVariable "sirenMode"}) exitWith {};

_firstRun = true;

/*
    Siren Modes:
        1: Wind Up and Run Loop
        2: Wind Down (Warning) and turn siren off
        3: Run Piercer for 4 seconds
*/

for "_i" from 0 to 1 step 0 do {
    if (!(_vehicle getVariable "siren")) exitWith {};
    if (count crew _vehicle isEqualTo 0) then {_vehicle setVariable ["siren", false, true]};
    if (!alive _vehicle) exitWith {};
    if (isNull _vehicle) exitWith {};

    switch (_vehicle getVariable "sirenMode") do {
        case 1: {
            if (_firstRun) then {
                _vehicle say3D "windUpSiren";
                sleep 3.25; // Exactly matches the length of the audio file.
                _firstRun = false;
                if (!(_vehicle getVariable "siren")) exitWith {};
            } else {
                _vehicle say3D "sirenLoop";
                sleep 2; // Exactly matches the length of the audio file.
                if (!(_vehicle getVariable "siren")) exitWith {};
            };
        };

        case 2: {
            _vehicle say3D "windDownSiren";
            _vehicle setVariable ["siren", false, true];
            _vehicle setVariable ["sirenMode", 1, true];
            sleep 3; // Exactly matches the length of the audio file.
            if (!(_vehicle getVariable "siren")) exitWith {};
        };

        case 3: {
            _vehicle say3D "sirenPiercer";
            _vehicle setVariable ["sirenMode", 1, true];
            sleep 4; // Exactly matches the length of the audio file.
            if (!(_vehicle getVariable "siren")) exitWith {};
        };
    };


};
