/*
    File: fn_sirenLights.sqf
    Author: Garrett
    Description:
        Starts the lights on the vehicle
*/

private ["_vehicle", "_"];
_vehicle = param [0,objNull,[objNull]];
if (isNull _vehicle) exitWith {}; //Bad entry!
if (!(typeOf _vehicle in ["B_GEN_Offroad_01_gen_F", "C_Offroad_01_F","B_MRAP_01_F","C_SUV_01_F","C_Hatchback_01_sport_F","B_Heli_Light_01_F","C_Heli_Light_01_civil_F","B_Heli_Transport_01_F"])) exitWith {};

_trueorfalse = _vehicle getVariable ["lights",false];

if (_trueorfalse) then {
    _vehicle setVariable ["lights",false,true];
} else {
    _vehicle setVariable ["lights",true,true];
    [_vehicle, 0.22] remoteExec ["cop_fnc_copLights", -2];
};
