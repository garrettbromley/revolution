/*
	File: fn_policeMarkers.sqf
	Author: Garrett
	Description:
		Creates/Deletes the police only markers
*/

private ["_positions", "_marker"];
params ["_choice"];

_positions = [[6962.252, 7171.169, "Police Training Grounds"]];

// Create them
if (_choice) then {
	policeMarkers = [];
	{
		_marker = createMarkerLocal [format["police_%1_marker", _forEachIndex], [(_x select 0), (_x select 1)]];
		_marker setMarkerColorLocal "ColorBLUFOR";
		_marker setMarkerTypeLocal "Mil_box";
		_marker setMarkerTextLocal format["%1", (_x select 2)];
		policeMarkers pushBack _marker;
	} forEach _positions;
} else {
	// Delete them
	if (isNil "policeMarkers") exitWith {};
	{deleteMarkerLocal (_x); } forEach policeMarkers;
	policeMarkers = nil;	// Clean up
};
