/*
    File: fn_statusBar.sqf
    Author: Garrett
    Description:
        Runs the status bar
*/

waitUntil {!(isNull (findDisplay 46))};
disableSerialization;

_rscLayer = "osefStatusBar" call BIS_fnc_rscLayer;
_rscLayer cutRsc["osefStatusBar", "PLAIN"];

[] spawn {
	sleep 5;
	_counter = 180;
	_timeSinceLastUpdate = 0;
	while {true} do
	{
		sleep 1;
		_counter = _counter - 1;
		((uiNamespace getVariable "osefStatusBar")displayCtrl 1000)ctrlSetText format["FPS: %1 | CASH: %2 | BANK: %3 | ON-DUTY COPS: %4 | CIVILIANS: %5 | GRIDREF: %6", round diag_fps, cash, bank, {_x getVariable ["isCop", false]} count playableUnits, {!(_x getVariable ["isCop", false])} count playableUnits, mapGridPosition player];
	};
};
