/*
	File: fn_loadSpeedCameras.sqf
	Author: Garrett
	Description:
		I'll let you think about this...
*/

private["_object","_positions"];

// The server will create the objects
if(isServer) exitWith {
	speedcameras = [];
    {
        _object = createSimpleObject [((str missionConfigFile select [0, count str missionConfigFile - 15]) + "models\speedCamera.p3d"), (_x select 0)];
        _object setPosATL (_x select 0);
        _object setDir (_x select 1);
		speedcameras pushBack _object;
    } forEach (getArray(missionConfigFile >> "MissionCFGMain" >> "SpeedCameras" >> "SpeedCameras"));
	publicVariable "speedcameras";
	diag_log "Speed Cameras Spawned";
};

// Store the positions of the cameras
_positions = [];
{_positions pushBack (_x select 0);} forEach (getArray(missionConfigFile >> "MissionCFGMain" >> "SpeedCameras" >> "SpeedCameras"));

waitUntil {!isNil "activeRole"};

// If there are cameras, run this
if(count _positions > 0 && activeRole != "cop") exitWith {
    [_positions] spawn {

		// Setup
        private["_positions","_speed"];
        _positions = (_this select 0);
        isSpeeding = false;

        while {true} do {

			// Wait for them to be a driver
            waitUntil{((vehicle player) != (player)) && ((driver (vehicle player)) == (player))};

            {
				// If they are approaching the camera
				if(((getPosATL player) distance2D _x) <= 40 && !isSpeeding && ((((vehicle player) getRelDir (speedcameras select _ForEachIndex)) > 1) && (((vehicle player) getRelDir (speedcameras select _ForEachIndex)) < 120)) && (((speedcameras select _ForEachIndex) getRelDir (vehicle player)) < 120 && ((speedcameras select _ForEachIndex) getRelDir (vehicle player)) > 40)) then {

					// Get the speed from the config 															Give them a buffer of 10 km/h over
                    _speed = (((getArray(missionConfigFile >> "MissionCFGMain" >> "SpeedCameras" >> "SpeedCameras")) select _ForEachIndex) select 2) + 10;

					// If they are speeding			And double sure they aren't a cop
                    if((speed (vehicle player)) > _speed && activeRole != "cop") then {

						// Set the trigger
						isSpeeding = true;

						// Flash the lights!
						"colorCorrections" ppEffectEnable true;
						"colorCorrections" ppEffectAdjust [1, 15, 0, [0.5, 0.5, 0.5, 0], [0.0, 0.5, 0.0, 1],[0.3, 0.3, 0.3, 0.05]];
						"colorCorrections" ppEffectCommit 0;
						"colorCorrections" ppEffectAdjust [1, 1, 0, [1, 1, 1, 0.0], [1, 1, 1, 1],  [1, 1, 1, 1]];
						"colorCorrections" ppEffectCommit 0.05;
						sleep 0.1337;
						"colorCorrections" ppEffectAdjust [1, 15, 0, [0.5, 0.5, 0.5, 0], [0.0, 0.5, 0.0, 1],[0.3, 0.3, 0.3, 0.05]];
						"colorCorrections" ppEffectCommit 0;
						"colorCorrections" ppEffectAdjust [1, 1, 0, [1, 1, 1, 0.0], [1, 1, 1, 1],  [1, 1, 1, 1]];
						"colorCorrections" ppEffectCommit 0.05;
						sleep 0.234;
						"colorCorrections" ppEffectEnable false;

						// Setup the text to display
						private _text = "<t color='#cc1f00'><t size='2'><t align='center'>Speed Camera</t></t><t color='#ffffff'><br/><br/>";
						_text = _text + format[(getText(missionConfigFile >> "MissionCFGMain" >> "SpeedCameras" >> "InfoMSG")), round((speed (vehicle player)) - _speed)];

						private _speedingFee = (getNumber(missionConfigFile >> "MissionCFGMain" >> "SpeedCameras" >> "SpeedingFee"));
						_text = _text + "<br/>" + format[(getText(missionConfigFile >> "MissionCFGMain" >> "SpeedCameras" >> "SpeedingFeeMSG")), _speedingFee];

						// Add the fee to the user
						fines = fines + _speedingFee;

						// Add the fine in the database
						[_speedingFee] call fines_fnc_addFine;

						// Send the hint to the user
						hint parseText _text;

						// Reset the trigger
						[] spawn {
							uiSleep 10;
							isSpeeding = false;
						};
                    };
                };
            } forEach _positions;

            uiSleep 1.2;
        };
    };
};
