/*
	Author: Jannis (Tschuuut)

	Description:
	Clamps number between to numbers

	Parameter(s):
		value:					number - the number that will be clamped
		minValue (optional):	number - minimum		(inclusive)
		maxValue (optional):	number - maximum value	(inclusive

	Returns:
		number - clampedValue
*/
private ["_value","_min","_max"];
_value = param [0, 0, [0]];
_min = param [1, 0, [0]];
_max = param [2, 1, [0]];

if(_value > _max) exitWith {_max};
if(_value < _min) exitWith {_min};
_value;