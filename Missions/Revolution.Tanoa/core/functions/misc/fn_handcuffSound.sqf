/*
    File: fn_handcuffSound.sqf
    Author: Garrett
    Description:
        I think this plays the sound of the hand cuffs? Not sure haha
*/

private ["_source"];
_source = [_this,0,objNull,[objNull]] call BIS_fnc_param;
if (isNull _source) exitWith {};
_source say3D "cuffSound";
