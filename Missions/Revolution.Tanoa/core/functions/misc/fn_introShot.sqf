/*
    File: fn_introShot.sqf
    Author: Garrett
    Description:
        Displays the intro to the player upon loading in
*/

videoStarted = false;

// Setup event handler 
private _videoEH = [missionNamespace, "BIS_fnc_playVideo_started", {videoStarted = true;}] call BIS_fnc_addScriptedEventHandler; 

// Hide me!
[player, true] remoteExec ["hideObjectGlobal", 2];
disableUserInput true;

waitUntil {
    !(isNull (findDisplay 46)); 
    private _isLoading = [] call BIS_fnc_isLoading; 
    !_isLoading
};

// Make sure we are faded
titleCut ["", "BLACK FADED", 999];

// Play the video
["a3\missions_f_exp\video\exp_m04_vin.ogv"] spawn BIS_fnc_playVideo;

waitUntil { videoStarted };

// Play the music
0 fadeMusic 1;
playMusic "LeadTrack06b_F_EPC";

// Wait for the video to end
uiSleep 27.4;

// End video
[""] spawn BIS_fnc_playVideo;

// Unhide me!
[player, false] remoteExec ["hideObjectGlobal", 2];
disableUserInput false;

// Start Fading Music
15 fadeMusic 0;

// Fade in
[
	["Welcome to the Revolution","font = 'PuristaSemiBold'"],
	["","<br/>"],
	["Whose side are you on?","font = 'PuristaLight'"]
]  execVM "\a3\missions_f_bootcamp\Campaign\Functions\GUI\fn_SITREP.sqf";

uiSleep 3;
"dynamicBlur" ppEffectEnable true;
"dynamicBlur" ppEffectAdjust [6];
"dynamicBlur" ppEffectCommit 0;
"dynamicBlur" ppEffectAdjust [0.0];
"dynamicBlur" ppEffectCommit 5;
titleCut ["", "BLACK IN", 5];

// End music at the end of the fade
uiSleep 12;
playMusic "";
0 fadeMusic 1;

// Clean up
[missionNamespace, "BIS_fnc_playVideo_started", _videoEH] call BIS_fnc_removeScriptedEventHandler;
videoStarted = nil;