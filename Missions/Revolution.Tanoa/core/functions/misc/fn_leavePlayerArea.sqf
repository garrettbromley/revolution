/*
	File: fn_leavePlayerArea.sqf 
	Author: Garrett 
	Description:
		Handles a player leaving the play area
*/

if (isServer) exitWith {};

// If they are in an air vehicle, exclude them from running this
if (vehicle player isKindOf "Air" || inPlayArea) exitWith {};

private ["_handle", "_name", "_priority", "_effect", "_start", "_buffer"];

// Gather starting information
_start = time;
_buffer = (MissionConfigFile >> "MissionCFGMain" >> "Master_Settings" >> "timeToReturn") call BIS_fnc_getCfgData;

// Effect Parameters
_name = "ColorCorrections";
_priority = 1500;
_effect = [
	1, 
	0.4, 
	0, 
	0, 0, 0, 0, 
	1, 1, 1, 0, 
	1, 1, 1, 0
];

// Create the effect
while {
	_handle = ppEffectCreate [_name, _priority];
	_handle < 0
} do {
	_priority = _priority + 1;
};

_handle ppEffectEnable true;
_handle ppEffectAdjust _effect;
_handle ppEffectCommit 5;

// Wait until they are either back in the play area or they run out of time
while {!inPlayArea && time - _start <= _buffer} do {
	private _timeLeft = _buffer - (time - _start);
	cutText [format ["RETURN TO THE PLAYER AREA IMMEDIATELY\nYOU HAVE %1 SECONDS REMAINING", round _timeLeft], "PLAIN"];

	// Run every second
	uiSleep 1;
};

if (inPlayArea) exitWith { _handle ppEffectEnable false; ppEffectDestroy _handle; cutText ["", "PLAIN"];};

// Kill them
cutText ["MAYBE YOU WILL LISTEN NEXT TIME...", "PLAIN"];
uiSleep 1;
"Bo_GBU12_LGB" createVehicle [getPos player select 0, getPos player select 1];
_handle ppEffectEnable false; ppEffectDestroy _handle;