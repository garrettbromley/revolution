/*
	Author: Jannis (Tschuuut)

	Description:
	Returns number as formated string (1234411.4321 => 1,234,411.4321)

	Parameter(s):
		number:					number - the number that will be returned
		decimals (optional):	number - how many decimals the string should have (default: 0)

	Returns:
		nothing
*/
private ["_number","_decimals","_return"];

_decimals =  [param [1,0,[0]], 0, 5] call Misc_fnc_clamp;
_number = [floor((param [0,0,[0]]) * (10 ^ _decimals))] call BIS_fnc_numberDigits;

_return = "";
_dig = 0;
_decimals = count _number - _decimals;

for "_i" from (count _number - 1) to 0 step -1 do {
	if(_dig > 2) then {_return = "," + _return; _dig = 0;};
	_return = str(_number select _i) + _return;
	if(_i < _decimals) then {_dig = _dig + 1};
	if(_i == _decimals) then {_return = "." + _return;};
	
};

if(_decimals == 0) then {_return = "0" + _return;};

_return;