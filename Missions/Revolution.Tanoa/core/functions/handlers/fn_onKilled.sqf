/*
    File: fn_onKilled.sqf
    Author: Garrett
    Description:
        Well, the player died, so now what...
*/

private ["_theKiller"];

params [
    ["_unit",objNull,[objNull]],
    ["_killer",objNull,[objNull]],
    ["_instigator",objNull,[objNull]]
];
disableSerialization;

// Set the true killer
if (isNull _instigator) then {
    _theKiller = _killer;
} else {
    _theKiller = _instigator;
};

// Remove them from the vehicle they are in
if  !((vehicle _unit) isEqualTo _unit) then {
    UnAssignVehicle _unit;
    _unit action ["getOut", vehicle _unit];
    _unit setPosATL [(getPosATL _unit select 0) + 3, (getPosATL _unit select 1) + 1, 0];
};

// Remove their weapons 
_unit removeWeapon (primaryWeapon _unit);
_unit removeWeapon (handgunWeapon _unit);
_unit removeWeapon (secondaryWeapon _unit);

// Set some variables
player setVariable ["Restrained", false, true];
player setVariable ["Escorting", false, true];
cash = 0;
isAlive = 0;

[] call update_fnc_money;

// If they killed themself
if (_unit == _theKiller) exitWith {};

// If a cop killed them
if (_theKiller getVariable "isCop") exitWith {};