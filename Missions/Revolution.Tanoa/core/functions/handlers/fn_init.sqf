waitUntil { !isNull player && player == player; };

player addEventHandler["Killed", { _this call handle_fnc_onKilled; }];
player addEventHandler["handleDamage", { _this call handle_fnc_handleDamage; }];
//player addEventHandler["Take", {_this call handle_fnc_onTakeItem}]; //Prevent people from taking stuff they shouldn't...
//player addEventHandler["Fired", {_this call handle_fnc_onFired}];
//player addEventHandler["InventoryClosed", {_this call handle_fnc_inventoryClosed}];
//player addEventHandler["InventoryOpened", {_this call handle_fnc_inventoryOpened}];
