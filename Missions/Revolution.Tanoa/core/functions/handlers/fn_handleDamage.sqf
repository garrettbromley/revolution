/*
    File: fn_handleDamage.sqf
    Author: Garrett
    Description:
        Handles damage, just for tazer
*/

params [
    ["_unit",objNull,[objNull]],
    ["_part","",[""]],
    ["_damage",0,[0]],
    ["_source",objNull,[objNull]],
    ["_projectile","",[""]],
    ["_index",0,[0]]
];

if (!isNull _source) then {
    if (_source != _unit) then {
        if (currentWeapon _source in ["hgun_P07_F","arifle_SDAR_F"] && _projectile in ["B_9x21_Ball","B_556x45_dual"]) then {
            if (_source getVariable ["isCop", false] && activeRole == "civ") then {
                _damage = 0;
                if (alive player && !isTazed && !isKnockedOut && !(_unit getVariable ["Restrained", false])) then {
                    private ["_distance"];
                    _distance = 35;
                    if (_projectile == "B_556x45_dual") then { _distance = 100; };
                    if (_unit distance _source < _distance) then {
                        if !(isNull objectParent player) then {
                            if (typeOf (vehicle player) == "B_Quadbike_01_F") then {
                                player action ["Eject", vehicle player];
                                [_unit, _source] spawn civ_fnc_tazed;
                            };
                        } else {
                            [_unit, _source] spawn civ_fnc_tazed;
                        };
                    };
                };
            };

            //Temp fix for super tasers on cops.
            /*if (side _source isEqualTo west && (playerSide isEqualTo west || playerSide isEqualTo independent)) then {
                _damage = 0;
            };*/
        };
    };
};

_damage;
