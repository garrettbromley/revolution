/*
    File: fn_keyHandler.sqf
    Author: Garrett
    Description:
        Handles the keys the user presses
*/

indicators_lasttick = -999999;	// For indicators to work

disableSerialization;

private ["_handled","_shift","_alt","_code","_ctrl","_alt","_ctrlKey","_veh","_locked","_interactionKey","_mapKey","_interruptionKeys"];
params [
	["_D", objNull],
	["_code", objNull],
	["_shift", objNull],
	["_ctrlKey", objNull],
	["_alt", objNull]
];
_handled = false;

_interactionKey = if (count (actionKeys "User10") isEqualTo 0) then {219} else {(actionKeys "User10") select 0};
_mapKey = (actionKeys "ShowMap" select 0);
_interruptionKeys = [17,30,31,32]; //A,S,W,D

// Vault handling...
if ((_code in (actionKeys "GetOver") || _code in (actionKeys "salute") || _code in (actionKeys "SitDown") || _code in (actionKeys "Throw") || _code in (actionKeys "GetIn") || _code in (actionKeys "GetOut") || _code in (actionKeys "Fire") || _code in (actionKeys "ReloadMagazine") || _code in [16,18]) && ((player getVariable ["restrained",false]) || isKnockedOut || isTazed)) exitWith { true; };

switch (_code) do {
    // 1 Key (Sirens - Wind up and Loop)
    case 2: {
        if (activeRole == "cop" && {vehicle player != player} && {!sirenOn} && {((driver vehicle player) == player)}) then {
            [] spawn {
                sirenOn = true;
                sleep 3;
                sirenOn = false;
            };

            /*
                Siren Modes:
                    1: Wind Up and Run Loop
                    2: Wind Down (Warning) and turn siren off
                    3: Run Piercer for 4 seconds
            */

            _veh = vehicle player;
            if (isNil {_veh getVariable "siren"}) then {_veh setVariable ["siren", false, true];};
            _veh setVariable ["sirenMode", 1, true];
            if ((_veh getVariable "siren")) then {
                titleText ["Sirens Off", "PLAIN"];
                _veh setVariable ["siren", false, true];
            } else {
                titleText ["Sirens On", "PLAIN"];
                _veh setVariable ["siren", true, true];
                [_veh] remoteExec ["cop_fnc_siren", -2];
            };
        };
    };

    // 2 Key (Siren Mode 2 - Wind down and off)
    case 3: {
        if (activeRole == "cop" && {vehicle player != player} && {!sirenOn} && {((driver vehicle player) == player)}) then {
            [] spawn {
                sirenOn = true;
                sleep 2;
                sirenOn = false;
            };

            _veh = vehicle player;
            if (isNil {_veh getVariable "siren"}) then {_veh setVariable ["siren", false, true];};
            _veh setVariable ["sirenMode", 2, true];
            if ((_veh getVariable "siren")) then {
                if (_veh getVariable "sirens") then { titleText [localize "Winding Down Sirens","PLAIN DOWN"]; };
            } else {
                titleText ["Sounding Warning", "PLAIN"];
                _veh setVariable ["siren", true, true];
                [_veh] remoteExec ["cop_fnc_siren", -2];
            };




        };
    };

    // 3 Key (Siren Mode 3 - Run piercer for 4 seconds)
    case 4: {
        if (activeRole == "cop" && {vehicle player != player} && {!sirenOn} && {((driver vehicle player) == player)}) then {
            [] spawn {
                sirenOn = true;
                sleep 2;
                sirenOn = false;
            };

            _veh = vehicle player;
            _veh setVariable ["sirenMode", 3, true];
            if (_veh getVariable "sirens") then { titleText ["Running Piercer", "PLAIN"]; };
        };
    };

    // Holster / recall weapon. (4)
    case 5: {
        if (!(currentWeapon player isEqualTo "")) then {
            curWeapon = currentWeapon player;
            player action ["SwitchWeapon", player, player, 100];
            player switchCamera cameraView;
        };
    };

	//Surrender (Shift + B)
    case 48: {
        if (_shift) then {
            if (player getVariable ["surrender",false]) then {
                player setVariable ["surrender",false,true];
            } else {
                [] spawn actions_fnc_surrender;
            };
            _handled = true;
        };
    };

    // Map Key
    case _mapKey: {
        if (activeRole == "cop") then {
            [] spawn cop_fnc_copMarkers;
        };
    };

	// Left Indicators
	case 16: {
		if (vehicle player != player && (driver (vehicle player)) == player && (diag_tickTime - indicators_lasttick) > 1) then {
			indicators_lasttick = diag_tickTime;
			[vehicle player, "left"] remoteExec ["indicators_fnc_enableIndicator"];
		};
	};

	// Right Indicators
	case 18: {
		if (vehicle player != player && (driver (vehicle player)) == player && (diag_tickTime - indicators_lasttick) > 1) then {
			indicators_lasttick = diag_tickTime;
			[vehicle player, "right"] remoteExec ["indicators_fnc_enableIndicator"];
		};
	};

	// Warn Indicators
	case 34: {
		if (vehicle player != player && (driver (vehicle player)) == player && (diag_tickTime - indicators_lasttick) > 1) then {
			indicators_lasttick = diag_tickTime;
			[vehicle player, "warn"] remoteExec ["indicators_fnc_enableIndicator"];
		};
	};

    // Restraining (Shift + R)
    case 19: {
        if (_shift) then {_handled = true;};
        if (_shift && activeRole == "cop" && {!isNull cursorObject} && {cursorObject isKindOf "Man"} && {(isPlayer cursorObject)} && {!(cursorObject getVariable ["isCop", false])} && {alive cursorObject} && {cursorObject distance player < 3.5} && {!(cursorObject getVariable ["Escorting", false])} && {!(cursorObject getVariable ["Restrained", false])} && {speed cursorObject < 1}) then {
            [] call cop_fnc_restrainAction;
        };
    };

    // L Key
    case 38: {
        // If cop run checks for turning lights on.
        if (_shift && activeRole == "cop") then {
            private ["_veh"];
            _veh = vehicle player;
            if (!(isNull objectParent player) && (typeOf _veh) in ["B_GEN_Offroad_01_gen_F", "C_Offroad_01_F","B_MRAP_01_F","C_SUV_01_F","C_Hatchback_01_sport_F","B_Heli_Light_01_F","C_Heli_Light_01_civil_F","B_Heli_Transport_01_F"]) then {
                if (isNil {_veh getVariable "lights"}) then {_veh setVariable ["lights", false, true];};
                if (!isNil {_veh getVariable "lights"}) then {
                    [_veh] call cop_fnc_sirenLights;
                    _handled = true;
                };
            };
        };

        if (!_alt && !_ctrlKey) then { [] call cop_fnc_radar; };
    };

    // End Key (Ear Plugs)
    case 207: {
        if !(soundVolume isEqualTo 1) then {
            1 fadeSound 1;
            titleText["Ear Plugs Out","PLAIN"];
        } else {
            1 fadeSound 0.1;
            titleText["Ear Plugs In","PLAIN"];
        };
    };
	
	//Y Key (Inventory key)
	case 21: {
		[] call inv_fnc_open
	};
};

_handled;
