#include "Keycodes.h"
disableSerialization;

_ctrl = _this select 0;
_dikCode = _this select 1;
_shift = _this select 2;
_ctrlKey = _this select 3;
_alt = _this select 4;

switch (_dikCode) do {

	// Numeric - Added to delete group behaviour (anoying dialong when pressing numbers)

	case DIK_0: {
	
	};
	
	case DIK_1: {
	
	};
	
	case DIK_2: {
	
	};
	
	case DIK_3: {
	
	};
	
	case DIK_4: {
	
	};
	
	case DIK_5: {
	
	};
	
	case DIK_6: {
	
	};
	
	case DIK_7: {
	
	};
	
	case DIK_8: {
	
	};
	
	case DIK_9: {
	
	};
	
	//System Keys
	
	case DIK_F1: {
	
	};
	
	case DIK_F2: {
		createDialog "RscDisplayDebugPublic";
	};
	
	case DIK_F3: {
	
	};
	
	case DIK_F4: {
	
	};
	
	case DIK_F5: {
	
	};
	
	case DIK_F6: {
	
	};
	
	case DIK_F7: {
	
	};
	
	case DIK_F8: {
	
	};
	
	case DIK_F9: {
	
	};
	
	case DIK_F10: {
	
	};
	
	case DIK_F11: {
	
	};
	
	case DIK_F12: {
	
	};
	
	// Special
	case DIK_GRAVE: {
	
	};
	
	//QWERTY
	
	case DIK_F : {
	
	};
	case DIK_M : {
		if(!dialog) then {
			createDialog "MapDialog";
		};
	};
	
	// Others
	case DIK_ESCAPE : {
		if(dialog && !(missionNamespace getVariable["cantClose", false])) then {
			closeDialog 0;
		} else {
			createDialog "PauseDialog";
		};
	};
	
	default {
	
	};
};