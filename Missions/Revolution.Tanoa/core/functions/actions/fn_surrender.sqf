/*
    File: fn_surrender.sqf
    Author: Garrett
    Description:
        Causes the player to surrender
*/

if ( player getVariable ["Restrained",false] ) exitWith {};
if ( player getVariable ["Escorting",false] ) exitWith {};
if ( vehicle player != player ) exitWith {};
if ( speed player > 1 ) exitWith {};

if (player getVariable ["surrender",false]) then {
    player setVariable ["surrender",false,true];
} else {
    player setVariable ["surrender",true,true];
};

while {player getVariable ["surrender",false]} do {
    player playMove "AmovPercMstpSnonWnonDnon_AmovPercMstpSsurWnonDnon";
    if (!alive player || !(isNull objectParent player)) then { player setVariable ["surrender",false,true]; };
};

player playMoveNow "AmovPercMstpSsurWnonDnon_AmovPercMstpSnonWnonDnon";
