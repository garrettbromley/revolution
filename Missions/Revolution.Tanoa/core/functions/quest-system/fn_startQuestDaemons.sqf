if(missionNamespace getVariable["questSystemInitialized", false]) exitWith {
	diag_log "# Error: Trying to reinitialize quest system.";
};

[] spawn {
	questSystemRunning = true;

	while {questSystemRunning} do {
		if((missionnamespace getVariable["currentMission", -1]) > 0) then {
			
		};
		sleep 0.5;
	};
}