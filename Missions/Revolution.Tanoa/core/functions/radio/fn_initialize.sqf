/*
    File: fn_initialize.sqf
    Author: Garrett
    Description:
        Initialize the custom radio channels
        For when a player spawns in for the first time
*/

private ["_initCiv", "_initCop", "_initMed"];

_initCiv = {
    {
        _x radioChannelAdd [player];
    } forEach CIV_CHANNELS;
};

_initCop = {
    {
        _x radioChannelAdd [player];
    } forEach COP_CHANNELS;
};

_initMed = {
    {
        _x radioChannelAdd [player];
    } forEach MED_CHANNELS;
};

switch (activeRole) do {
    case "civ": { [] call _initCiv; };
    case "cop": { [] call _initCop; };
    case "med": { [] call _initMed; };
};
