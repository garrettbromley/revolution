/*
    File: fn_change.sqf
    Author: Garrett
    Description:
        Removes old channels and adds new channels
        For when a player changes roles
*/

params ["_toRemove", "_toAdd"];

private ["_toCiv", "_fromCiv", "_toCop", "_fromCop", "_toMed", "_fromMed"];

_toCiv = {
    {
        _x radioChannelAdd [player];
    } forEach CIV_CHANNELS;
};

_fromCiv = {
    {
        _x radioChannelRemove [player];
    } forEach CIV_CHANNELS;
};

_toCop = {
    {
        _x radioChannelAdd [player];
    } forEach COP_CHANNELS;
};

_fromCop = {
    {
        _x radioChannelRemove [player];
    } forEach COP_CHANNELS;
};

_toMed = {
    {
        _x radioChannelAdd [player];
    } forEach MED_CHANNELS;
};

_fromMed = {
    {
        _x radioChannelRemove [player];
    } forEach MED_CHANNELS;
};

switch (_toRemove) do {
    case "civ": { [] call _fromCiv; };
    case "cop": { [] call _fromCop; };
    case "med": { [] call _fromMed; };
};

switch (_toAdd) do {
    case "civ": { [] call _toCiv; };
    case "cop": { [] call _toCop; };
    case "med": { [] call _toMed; };
};
