/*
  File: init.sqf
  Author:
  Description:
    Initialize the server and required systems.
*/

private ["_timeToSkip"];

// Initialize the Database
diag_log "Connecting to Database";
["revolution", "SQL_CUSTOM", "revolution"] call extDB_fnc_initialize;

// Setup the radio channels
[] call systems_fnc_setupRadio;
diag_log "Radio Channels Setup";

// Event handler for connecting/disconnecting players
connectedPlayers = [];  // [[playerID, time]]
addMissionEventHandler ["PlayerConnected", { _this call systems_fnc_clientConnect;          }];
addMissionEventHandler ["HandleDisconnect", {_this call systems_fnc_clientDisconnect; false;}];
diag_log "Mission Handlers Setup";

// Randomize time
waitUntil {time > 0};

// Disable gas pumps across Tanoa
[] call systems_fnc_disableGasPumps;
diag_log "Gas Pumps Disabled";

diag_log "Spawning Bridges";
[] call bridges_fnc_bridgeConfig;

diag_log "Setting up Weather/Time Management";
[] spawn systems_fnc_weather;

diag_log "Setting up Ambience";
[] spawn systems_fnc_ambience;

// Misc Settings
setTerrainGrid 50;
setViewDistance 2500;
setObjectViewDistance 2500;
