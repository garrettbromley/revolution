serverToClient_fnc_clientWeather = {
	private ["_mintime", "_maxtime", "_realtime", "_random","_startingdate", "_startingweather", "_daytimeratio", "_nighttimeratio", "_timesync", "_wind"];
	_realtime = false;
	_random = true;
	_mintime = 600;
	_maxtime = 1200;
	_daytimeratio = 6;
	_nighttimeratio = 24;
	_timesync = 60;
	_startingdate = [2018, 01, 01, 07, 00];
	_startingweather = ["CLEAR", "CLOUDY", "RAIN"] call BIS_fnc_selectRandom;

	setdate _startingdate;
	switch(toUpper(_startingweather)) do {
		case "CLEAR": {
			wcweather = [0, 0, 0, [random 3, random 3, true], date];
		};
			
		case "CLOUDY": {
			wcweather = [0, 0, 0.6, [random 3, random 3, true], date];
		};
			
		case "RAIN": {
			wcweather = [1, 0, 1, [random 3, random 3, true], date];
		};

		default {
			wcweather = [0, 0, 0, [random 3, random 3, true], date];
			diag_log "Real weather: wrong starting weather";
		};
	};

	if (local player) then {
		wcweatherstart = true;
		"wcweather" addPublicVariableEventHandler {
			if(wcweatherstart) then {
				wcweatherstart = false;
				skipTime -24;
				86400 setRain (wcweather select 0);
				86400 setfog (wcweather select 1);
				86400 setOvercast (wcweather select 2);
				skipTime 24;
				simulweatherSync;
				setwind (wcweather select 3);
				setdate (wcweather select 4);
			}else{
				wcweather = _this select 1;
				60 setRain (wcweather select 0);
				60 setfog (wcweather select 1);
				60 setOvercast (wcweather select 2);
				setwind (wcweather select 3);
				setdate (wcweather select 4);
			};
		};
	};
};

publicVariable "serverToClient_fnc_clientWeather";