// WEST BRIDGE
WEST_BRIDGE_enabled = true;
WEST_BRIDGE_DO_remove = true; // Remove/hide trees and objects
WEST_BRIDGE_extra_objects = true; // Build extra objects
WEST_BRIDGE_map_markers = true; // Use map markers
WEST_BRIDGE_remove_trees_positions =
[
	[4213.44,8377.34,0.00149155],
	[4218.18,8379.27,0.00168896],
	[4168.59,8349.32,0.00143766],
	[4160.17,8344.85,0.00143814],
	[4080.51,8298.69,0.00143814],
	[4036.42,8273.41,0.00143862]
];

// SOUTH BRIDGE
SOUTH_BRIDGE_enabled = false; // SOUTH Main Bridge
SOUTH_BRIDGE_DO_remove = true; // Remove/hide trees ad objects
SOUTH_BRIDGE_extra_objects = true; // Build extra objects
SOUTH_BRIDGE_map_markers = true; // Use map markers
SOUTH_BRIDGE_remove_trees_positions = [];
SOUTH_BRIDGE_remove_objects =
[
	[[7153.93,4250.76,0],79708],
	[[7154.52,4251.42,0], 79713],
	[[7175.1,4256.3,0], 79759],
	[[7183.13,4242.67,0], 79756],
	[[7147.77,4222.68,0], 79740],
	[[7153.45,4240.81,0], 79734],
	[[7157.15,4254.6,0], 79564],
	[[7156.67,4254.61,0], 79684],
	[[7155.92,4254.62,0], 79701]
];

if (WEST_BRIDGE_enabled) then {  [] call bridges_fnc_westBridgeProps;  };
if (SOUTH_BRIDGE_enabled) then { [] call bridges_fnc_southBridgeProps; };
