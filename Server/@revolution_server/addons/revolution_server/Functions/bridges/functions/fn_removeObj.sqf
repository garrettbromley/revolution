params ["_object_list"];
{
	_pos = _x select 0;
	_object_id = _x select 1;
	_tmp_obj = _pos nearestObject _object_id;
	_tmp_obj hideObjectGlobal true;
	_tmp_obj hideObject true;
	_tmp_obj setdammage 1;
} foreach _object_list;
