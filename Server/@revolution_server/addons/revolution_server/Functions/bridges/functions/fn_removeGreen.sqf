params ["_positions", "_distance"];
{
	if (isNil "_distance") then { _distance = 10; };
    private["_obj"];
	_obj = (nearestTerrainObjects [_x, ["Tree","Bush"], _distance]);
	{
		_x setdammage 1;
		_x hideObject true;
		_x hideObjectGlobal true;
	} foreach _obj;
} forEach _positions;
