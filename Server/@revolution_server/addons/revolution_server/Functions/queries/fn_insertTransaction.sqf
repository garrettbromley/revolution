/*
    File: fn_insertTransaction.sqf
    Author: Garrett

    Description:
        Insert a new transaction into the database
*/

// Only the server runs this
if (!isServer) exitWith {};

// Setup the parameters
params ["_from", "_to", "_amount", "_description"];

// Call the query
[format ["addTransaction:%1:%2:%3:%4", _from, _to, _amount, _description]] call extDB_fnc_async;
