/*
    File: fn_updateFines.sqf
    Author: Garrett

    Description:
        Updates the player's fines in the database
*/

// Only the server runs this
if (!isServer) exitWith {};

// Setup the parameters
params ["_pid"];

// Call the Query
[format ["updateFines:%1", _pid]] call extDB_fnc_async;
