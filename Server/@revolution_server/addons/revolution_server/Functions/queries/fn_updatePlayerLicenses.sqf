/*
    File: fn_updatePlayerLicenses.sqf
    Author: Garrett

    Description:
        Updates the player's licenses in the database
*/

// Only the server runs this
if (!isServer) exitWith {};

// Setup the parameters
params ["_pid", "_civLicenses", "_copLicenses", "_medLicenses"];

// Call the Query
[format ["updatePlayerLicenses:%1:%2:%3:%4", format ["""%1""", _civLicenses], format ["""%1""", _copLicenses], format ["""%1""", _medLicenses], _pid]] call extDB_fnc_async;
