/*
    File: fn_getFines.sqf
    Author: Garrett

    Description:
        Gets the player's fines from the database
*/

// Only the server runs this
if (!isServer) exitWith {};

// Setup variables/parameters
private ["_result"];
params ["_pid", "_sender"];

// Call the query
_result = [format ["getFines:%1", _pid], 2] call extDB_fnc_async;

// Return it to the player who asked for it
[_result] remoteExecCall ["fines_fnc_getFines", (owner _sender)];
