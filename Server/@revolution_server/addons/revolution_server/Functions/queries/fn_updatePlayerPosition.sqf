/*
    File: fn_updatePlayerPosition.sqf
    Author: Garrett

    Description:
        Updates the player's position in the database
*/

// Only the server runs this
if (!isServer) exitWith {};

// Setup the parameters
params ["_pid", "_position"];

// Call the Query
[format ["updatePlayerPosition:%1:%2", _position, _pid]] call extDB_fnc_async;
