/*
    File: fn_insertPurchase.sqf
    Author: Garrett

    Description:
        Insert a new purchase into the database
*/

// Only the server runs this
if (!isServer) exitWith {};

// Setup the parameters
params ["_pid", "_shop", "_amount"];

// Call the query
[format ["addPurchase:%1:%2:%3", _pid, format ["""%1""", _shop], _amount]] call extDB_fnc_async;
