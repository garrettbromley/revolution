/*
    File: fn_updatePlayerRole.sqf
    Author: Garrett

    Description:
        Updates the player's role in the database
*/

// Only the server runs this
if (!isServer) exitWith {};

// Setup the parameters
params ["_pid", "_activeRole"];

// Run the query
[format ["updatePlayerRole:%1:%2", format ["""%1""", _activeRole], _pid]] call extDB_fnc_async;
