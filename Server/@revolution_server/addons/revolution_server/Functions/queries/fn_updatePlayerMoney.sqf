/*
    File: fn_updatePlayerMoney.sqf
    Author: Garrett

    Description:
        Updates the player's money in the database
*/

// Only the server runs this
if (!isServer) exitWith {};

// Setup the parameters
params ["_pid", "_cash", "_bank", "_creditBalance", "_creditLimit"];

// Call the Query
[format ["updatePlayerMoney:%1:%2:%3:%4:%5", _cash, _bank, _creditBalance, _creditLimit, _pid]] call extDB_fnc_async;
