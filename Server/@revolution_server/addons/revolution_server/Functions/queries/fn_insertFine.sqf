/*
    File: fn_insertFine.sqf
    Author: Garrett

    Description:
        Insert a new fine into the database
*/

// Only the server runs this
if (!isServer) exitWith {};

// Setup the parameters
params ["_pid", "_name", "_amount"];

// Call the query
[format ["addFine:%1:%2:%3", _pid, format ["""%1""", _name], _amount]] call extDB_fnc_async;
