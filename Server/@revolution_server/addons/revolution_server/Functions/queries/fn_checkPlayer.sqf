/*
    File: fn_checkPlayer.sqf
    Author: Garrett

    Description:
        Checks to see if the player exists and returns true/false
*/

// Only the server runs this
if (!isServer) exitWith {};

// Setup variables
private ["_result", "_playerExists"];
_playerExists = nil;

// Setup parameters
params ["_pid", "_sender"];

//Error checks
if (isNull _sender) exitWith { diag_log "Sender is Null"; };

// Call the query
_result = ([format ["checkPlayer:%1", _pid], 2] call extDB_fnc_async) select 0;

// If there is a result, then they exist
if (_result select 0) then
{
	_playerExists = true;
} else {
	_playerExists = false;
};

// Return the check status to the player who asked
[_playerExists] remoteExecCall ["Process_fnc_playerCheck", (owner _sender)];
