class CfgPatches {
    class revolution_server {
        units[] = {"C_man_1"};
        weapons[] = {};
        requiredAddons[] = {"A3_Data_F","A3_Soft_F","A3_Soft_F_Offroad_01","A3_Characters_F"};
        fileName = "revolution_server.pbo";
        author = "Tanoans: Revolution";
    };
};

class CfgFunctions {
    class MySQL_Database {
        tag = "extDB";
        class MySQL {
            file = "\revolution_server\Functions\database";
            class initialize {};
            class strip {};
            class async {};
        };
    };

    class Queries {
        tag = "Query";
        class Query {
            file = "\revolution_server\Functions\queries";
            class checkPlayer {};
            class insertPlayer {};
            class insertPurchase {};
            class insertTransaction {};
            class insertFine {};
            class getPlayerData {};
            class getFines {};
            class updatePlayerMoney {};
            class updatePlayerLicenses {};
            class updatePlayerGear {};
            class updatePlayerRole {};
            class updatePlayerPosition {};
            class updateAllPlayerData {};
            class updatePlaytime {};
            class updateFines {};
        };
    };

    class System {
        tag = "Systems";
        class Systems {
            file = "\revolution_server\Functions\systems";
            class setupRadio {};
            class clientConnect {};
            class clientDisconnect {};
            class disableGasPumps {};
            class ambience {};
            class weather {};
        };
    };

    class ServerToClient {
        tag = "Client";
        class Client {
            file = "\revolution_server\Functions\client";
            class clientWeather {};
        };
    };

    class Bridge {
        tag = "Bridges";
        class Bridges {
            file = "\revolution_server\Functions\bridges\functions";
            class build {};
            class removeGreen {};
            class removeObj {};
            class arrowToMap {};
            class drawToMap {};
            class writeToMap {};
            class writeToMapFont {};
        };

        class BridgeCode {
            file = "\revolution_server\Functions\bridges\code";
            class southBridgeProps {};
            class westBridgeProps {};
        };

        class BridgeInit {
            file = "\revolution_server\Functions\bridges";
            class bridgeConfig {};
        };
    };
};
